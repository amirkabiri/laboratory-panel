import React from 'react';
import Progress from "../Progress";
import classNames from 'classnames';

const backgrounds = ['bg-teal-300', 'bg-green-300', 'bg-orange-300', 'bg-pink-300'];

export default function ProgressGroup({ className, titles, percents, metas, ...props }){
    return (
        <div
            className={ className }
        >
            {
                titles.map((title, i) => (
                    <div
                        key={ i }
                        className={ classNames(
                            'w-full py-2 flex items-center',
                        ) }
                    >
                        <Progress
                            percent={ percents[i] }
                            background={ backgrounds[i] }
                        />
                        <div className="mr-4 w-40 text-gray-500">{ titles[i] }</div>
                        <div className="w-32 text-gray-500">{ percents[i] }%</div>
                        <div className="w-32 text-gray-500 font-light">{ metas[i] }</div>
                    </div>
                ))
            }
        </div>
    );
}