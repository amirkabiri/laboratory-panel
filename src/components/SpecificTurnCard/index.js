import React, {useEffect, useRef} from 'react';
import Card from "components/Card";
import classNames from 'classnames';
import Button from "components/Button";
import Escape from "icons/Escape";

export default function SpecificTurnCard({ className, ...props }){
    const inputRef = useRef();

    const onNumberButtonClick = ({ target }) => inputRef.current && (inputRef.current.value += target.innerText);
    const onEscapeButtonClick = () => inputRef.current && (inputRef.current.value = inputRef.current.value.slice(0, inputRef.current.value.length - 1));

    useEffect(() => {

    }, []);

    return (
        <Card
            className={ classNames(className, 'flex flex-col-reverse p-10 bg-primary-gradient items-center md:flex-row') }
            { ...props }
        >
            <div className="flex-1 flex justify-center">
                <div className="grid grid-rows-4 grid-cols-3 gap-y-6 pl-0 md:pl-3" style={{ width : 320, justifyItems : 'center' }}>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">3</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">2</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">1</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">6</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">5</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">4</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">9</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">8</button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">7</button>

                    <button onClick={ onEscapeButtonClick } className="w-20 h-20 flex items-center justify-center">
                        <Escape width={ 50 }/>
                    </button>
                    <button onClick={ onNumberButtonClick } className="border border-white rounded-full text-white text-3xl w-20 h-20 hover:bg-white hover:text-blue-500 transition-colors duration-300">0</button>
                </div>
            </div>
            <div className="flex-1 flex items-center flex-col pr-0 md:pr-3 mb-10 md:mb-0">
                <h4 className="text-white text-2xl">نوبت خاص</h4>
                <input
                    ref={ inputRef }
                    dir="ltr"
                    className="text-center my-10 text-5xl box-border w-full text-gray-500 py-4 px-10 rounded-full bg-white shadow-md"
                />
                <Button color="success" className="text-2xl py-8 w-full">فراخوان</Button>
            </div>
        </Card>
    );
}