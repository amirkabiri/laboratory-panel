import React from 'react';
import PieChart, { colors } from "components/PieChart";
import classNames from 'classnames';
import PropTypes from 'prop-types';

export default function PieChartWithHorizontalInfo({ className, items, percents, children, innerPie, metas, ...props }){
    return (
        <div
            className={ classNames(className, 'h-full flex flex-col md:flex-row') }
            { ...props }
        >
            <div className="px-5 flex flex-col items-center justify-center">
                <PieChart
                    percents={ percents }
                >{ innerPie }</PieChart>
                { children }
            </div>
            <div className="flex-1 flex">
                {
                    items.map((item, index) => (
                        <div
                            className="flex flex-col py-2 items-center justify-center w-full"
                            key={ index }
                        >
                            <div className="flex flex-col items-center">
                                <span className="rounded-full w-4 h-4 inline-block ml-1" style={{ background : colors[index] }}/>
                                <span className="text-sm text-gray-500 mt-3 mb-4">{ item }</span>
                            </div>
                            <span className="text-gray-500 mb-2">{ percents[index] }%</span>
                            <span className="text-sm text-gray-500 font-light">{ metas[index] }</span>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}

PieChartWithHorizontalInfo.propTypes = {
    items : PropTypes.array.isRequired,
    percents : PropTypes.array.isRequired,
    metas : PropTypes.array.isRequired,
};