import { ModalContext } from "contexts/Modal";
import React, { useContext } from "react";
import RemoveModal from "./RemoveModal";

export default function useRemoveModal(){
    const { open, close } = useContext(ModalContext);

    return {
        open : ({ title, onCancel, onDelete }) => open(
            <RemoveModal
                title={ title }
                onCancel={ onCancel || close }
                onDelete={ onDelete }
            />
        ),
        close
    };
}