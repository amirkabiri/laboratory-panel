import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";

export default function RemoveModal({ title, onCancel, onDelete }) {
    return (
        <>
            <span className="text-lg text-gray-500">{ title }</span>
            <div className="grid grid-cols-2 gap-2 mt-10">
                <Button onClick={ onCancel } color="gray">انصراف</Button>
                <Button onClick={ onDelete } color="error">مطمئن هستم</Button>
            </div>
        </>
    );
}

RemoveModal.propTypes = {
    title : PropTypes.string.isRequired
};