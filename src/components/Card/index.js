import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Loading from 'icons/Loading';

export default function Card({ position, loading, rounded, padding, shadow, background, className, children, ...props }){
    return (
        <div
            className={ classNames(className, padding, rounded, shadow, background, position) }
            { ...props }
        >
            { children }

            { loading && (
                <div
                    className={ classNames(
                        'absolute right-0 left-0 top-0 bottom-0 w-full h-full flex items-center justify-center',
                        rounded
                    ) }
                    style={{ background : 'rgba(255,255,255,.5)' }}
                >
                    <Loading className="stroke-current text-blue-500"/>
                </div>
            ) }
        </div>
    );
}

Card.defaultProps = {
    padding : 'p-6',
    background : 'bg-white ',
    rounded : 'rounded-lg',
    shadow : 'shadow-xl',
    position : 'relative',
    loading : false,
};
Card.propTypes = {
    loading : PropTypes.bool
};