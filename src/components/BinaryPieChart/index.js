import React, { useEffect, useRef } from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

export default function BinaryPieChart({ className, percent, size, lineWidth, children, ...props }){
    const canvasRef = useRef();

    useEffect(() => {
        if(!canvasRef.current) return;
        const { current: canvas } = canvasRef;
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.save();
        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.rotate(-90 * Math.PI / 180);
        ctx.lineWidth = lineWidth;
        ctx.lineCap = 'round';

        ctx.beginPath();
        ctx.strokeStyle = '#E9ECF2';
        ctx.arc(0, 0, canvas.width/2 - lineWidth/2, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.closePath();

        ctx.beginPath();
        ctx.strokeStyle = '#12DB9D';
        ctx.arc(0, 0, canvas.width/2 - lineWidth/2, 0, (percent * 2 * Math.PI) / 100);
        ctx.stroke();
        ctx.closePath();

        ctx.restore();
    }, [percent]);

    return (
        <div
            className={ classNames(className, styles.container) }
            { ...props }
        >
            <canvas
                className={ styles.canvas }
                ref={ canvasRef }
                width={ size }
                height={ size }
            />
            <div
                className={ styles.children }
                style={{
                    width : size - 2 * lineWidth,
                    height : size - 2 * lineWidth,
                }}
            >{ children }</div>
        </div>
    );
}

BinaryPieChart.defaultProps = {
    percent : 0,
    size : 100,
    lineWidth : 8
};
BinaryPieChart.propTypes = {
    percent : PropTypes.number,
    size : PropTypes.number,
    lineWidth : PropTypes.number,
};