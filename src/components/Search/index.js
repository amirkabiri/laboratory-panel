import React from 'react';
import SearchIcon from "icons/Search";
import classNames from 'classnames';

export default function Search({ color, padding, border, overflow, flex, style, className, ...props }){
    return (
        <div
            className={ classNames(
                className,
                border,
                overflow,
                flex
            ) }
            style={ style }
        >
            <SearchIcon className="w-10 mx-1" />
            <input
                type="text"
                className={ classNames(
                    padding,
                    'flex-1',
                    color
                ) }
                placeholder="اینجا جستجو کنید"
                { ...props }
            />
        </div>
    );
}

Search.defaultProps = {
    border : 'border border-gray-150',
    overflow : 'overflow-hidden',
    flex : 'flex items-center',
    padding : 'py-3',
    color : 'text-gray-500'
};