import React from 'react';
import { Line } from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';

defaults.global.defaultFontFamily = "iransans";

export default function LineChart({ xLabels, showXLabels, smsCounts, viewCounts }){
    const state = {
        labels: xLabels,
        datasets: [
            {
                label: 'SMS',
                fill: false,
                lineTension: 0,
                backgroundColor: 'green',
                borderColor: 'green',
                borderWidth: 2,
                data: smsCounts
            },{
                label: 'View',
                fill: false,
                lineTension: 0,
                backgroundColor: '#204BA0',
                borderColor: '#596781',
                borderWidth: 2,
                data: viewCounts
            },
        ]
    };
    return (
        <Line
            data={state}
            options={{
                legend:{
                    display : false
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: showXLabels
                        }
                    }],
                    yAxes: [
                        {
                            type: 'linear',
                            display: true,
                            position: 'right',
                            id: 'y-axis-2',
                            ticks: {
                                suggestedMin: 0,
                                // max: Math.max(...data),
                                // min: Math.min(...data),
                                // stepSize: 10,
                            }
                        },
                        {
                            type: 'linear',
                            display: true,
                            position: 'left',
                            id: 'y-axis-1',
                            ticks: {
                                max: 100,
                                min: 0,
                                stepSize: 50,
                                callback: function (value) {
                                    return (value / this.max * 100).toFixed(0) + '%';
                                },
                            }
                        },
                    ],
                }
            }}
        />
    );
}

LineChart.defaultProps = {
    showXLabels : true
};