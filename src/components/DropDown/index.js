import React, { useState } from 'react';
import classNames from 'classnames';

export default function DropDown({ overflow, padding, width, background, shadow, transition, handle: Handle, open: propOpen, children, className, ...props }){
    const [open, setOpen] = useState(propOpen);

    return (
        <div
            className={ classNames(
                'relative'
            ) }
            { ...props }
        >
            <Handle
                onClick={ () => setOpen(open => !open) }
            />
            <div style={{ marginTop : 7 }} className={ classNames(
                { 'opacity-100' : open, 'opacity-0 invisible' : !open },
                padding,
                width,
                overflow,
                background,
                shadow,
                transition,
                'left-0  absolute'
            ) }>
                { children }
            </div>
        </div>
    );
}

DropDown.defaultProps = {
    handle : props => (
        <div { ...props }>handle</div>
    ),
    open : false,
    overflow: 'overflow-hidden',
    shadow : 'shadow-lg',
    transition : 'transition-all duration-300',
    padding : 'p-4',
    width : 'w-40',
    background : 'bg-white',
};