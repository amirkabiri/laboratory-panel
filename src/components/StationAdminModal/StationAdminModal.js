import React, {useContext, useEffect, useState} from 'react';
import Button from "components/Button";
import Crown from "icons/Crown";
import Search from "components/Search";
import classNames from 'classnames';
import styles from './index.module.scss';
import {ModalContext} from "../../contexts/Modal";

export default function StationAdminModal({ }) {
    const [users, setUsers] = useState([]);
    const [search, setSearch] = useState('');
    const [selected, setSelected] = useState(-1);
    const { close: closeModal } = useContext(ModalContext);

    useEffect(() => {
        setUsers([
            { name : 'شایان احدی', username : 'shayan_ahadi', phone : '09123456789' },
            { name : 'فاطمه اقا میرزایی', username : 'fateme', phone : '09138302815' },
            { name : 'لادن شاپورزاده', username : 'ladan_sh', phone : '09140463287' },
        ]);
    }, []);

    return (
        <>
            <div className="px-6 md:px-12">
                <div className="flex items-center">
                    <Crown/>
                    <span className="text-lg text-gray-500 mr-3">انتخاب مدیر ایستگاه</span>
                </div>

                <Search
                    onChange={ ({ target }) => setSearch(target.value) }
                    className="my-5"
                />
            </div>

            {
                users
                    .filter(user => Object.keys(user).some(key => new RegExp(search).test(user[key])))
                    .map((user, index) => (
                    <div
                        key={ index }
                        className={ classNames(
                            'px-6 md:px-12 py-5 text-gray-500 border-b border-gray-150 hover:bg-gray-100 flex items-center justify-between',
                            styles.user
                        ) }
                    >
                        <div>
                            <span>{ user.name }</span>
                            <div className="flex items-center mt-2">
                                <span className="border-l border-gray-150 pl-4 ml-4">{ user.username }</span>
                                <span>{ user.phone }</span>
                            </div>
                        </div>
                        { selected === index && ( <Crown/> ) }
                        {
                            selected !== index && (
                                <Button
                                    color="success"
                                    padding="px-4 py-2"
                                    className={ styles.selectButton }
                                    onClick={ () => setSelected(index) }
                                >انتخاب</Button>
                            )
                        }
                    </div>
                ))
            }

            <div className="px-6 md:px-12 mt-6 md:mt-10 grid grid-cols-1 grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-3">
                <Button
                    color="gray"
                    onClick={ closeModal }
                >انصراف</Button>
                <Button color="success">ذخیره اطلاعات</Button>
            </div>
        </>
    );
}

StationAdminModal.propTypes = {

};