import { ModalContext } from "contexts/Modal";
import React, { useContext } from "react";
import StationAdminModal from "./StationAdminModal";

export default function useStationAdminModal(){
    const { open, close } = useContext(ModalContext);

    return {
        open : ({ onCancel, onSave }) => open(
            <StationAdminModal
                onCancel={ onCancel || close }
                onSave={ onSave }
            />
            ,{
                padding : 'py-6 md:py-12'
            }
        ),
        close
    };
}