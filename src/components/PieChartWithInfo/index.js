import React from 'react';
import PieChart, { colors } from "components/PieChart";
import classNames from 'classnames';
import PropTypes from 'prop-types';

export default function PieChartWithInfo({ className, items, percents, metas, ...props }){
    return (
        <div
            className={ classNames(className, 'h-full flex flex-col md:flex-row') }
            { ...props }
        >
            <div className="px-5 flex items-center justify-center">
                <PieChart
                    percents={ percents }
                />
            </div>
            <div className="flex-1 flex flex-col justify-around">
                {
                    items.map((item, index) => (
                        <div
                            className="flex items-center justify-between py-2"
                            key={ index }
                        >
                            <div className="flex items-center">
                                <span className="rounded-full w-4 h-4 inline-block ml-1" style={{ background : colors[index] }}/>
                                <span className="text-sm text-gray-500">{ item }</span>
                            </div>
                            <span className="text-sm text-gray-500 font-light">{ metas[index] }</span>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}

PieChartWithInfo.propTypes = {
    items : PropTypes.array.isRequired,
    percents : PropTypes.array.isRequired,
    metas : PropTypes.array.isRequired,
};