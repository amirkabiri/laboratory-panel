import React from 'react';
import classNames from 'classnames';

const types = {
    success: {
        icon: (
            <svg className="ml-1" xmlns="http://www.w3.org/2000/svg" width="10.488" height="9.082" viewBox="0 0 10.488 9.082">
                <path d="M-8434.521-22092.951l2.8,2.924,6.226-6.943" transform="translate(8435.243 22097.639)"
                      fill="none" stroke="#07d3b3" strokeWidth="2"/>
            </svg>
        ),
        color : 'bg-green-200 text-green-500'
    },
    error: {
        icon: (
            <svg className="ml-1" xmlns="http://www.w3.org/2000/svg" width="2" height="11.26" viewBox="0 0 2 11.26">
                <g transform="translate(1)">
                    <path d="M-8425.5-22089.83v-7.141" transform="translate(8425.499 22096.971)" fill="none"
                          stroke="#ff4e85" strokeWidth="2"/>
                    <path d="M-8425.5-22094.711v-2.26" transform="translate(8425.499 22105.971)" fill="none"
                          stroke="#ff4e85" strokeWidth="2"/>
                </g>
            </svg>
        ),
        color : 'bg-pink-200 text-pink-500'
    },
    warning: {
        icon: (
            <svg className="ml-1" xmlns="http://www.w3.org/2000/svg" width="9.192" height="9.192" viewBox="0 0 9.192 9.192">
                <g transform="translate(4.596 -3.182) rotate(45)">
                    <line y2="11" transform="translate(5.5)" fill="none" stroke="#f2b74a" strokeWidth="2"/>
                    <line x2="11" transform="translate(0 5.5)" fill="none" stroke="#f2b74a" strokeWidth="2"/>
                </g>
            </svg>
        ),
        color : 'bg-orange-200 text-orange-500'
    }
};

export default function Badge({ flex, display, text, rounded, color, padding, className, children, ...props }) {
    const type = types[color];

    return (
        <div
            className={classNames(
                className,
                display,
                text,
                rounded,
                padding,
                flex,
                type.color
            )}
        >
            { type.icon }
            {children}
        </div>
    );
}

Badge.defaultProps = {
    padding : 'py-1 px-3',
    rounded : 'rounded',
    text : 'text-sm font-medium',
    display : 'inline-flex',
    flex : 'items-center'
};
Badge.propTypes = {};