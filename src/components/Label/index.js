import React, {useState} from 'react';
import Close from "../../icons/Close";
import classNames from 'classnames';

export default function Label({ rounded, padding, display, fontWeight, color, border, className, children, ...props }){
    const [hover, setHover] = useState(false);

    return (
        <button
            className={ classNames(
                className,
                rounded,
                padding,
                display,
                fontWeight,
                color,
                border,
                { 'border-gray-200' : !hover, 'border-red-400' : hover }
            ) }
            onMouseEnter={ () => setHover(true) }
            onMouseLeave={ () => setHover(false) }
            { ...props }
        >
            { children }
            <div
                className={ classNames(
                    'p-2 rounded-full border absolute top-0 bottom-0 m-auto',
                    { 'border-gray-200' : !hover, 'border-red-400' : hover},
                    { 'bg-red-400' : hover},
                ) }
                style={{ left : 7, height : 'max-content' }}
            >
                <Close color={ hover ? '#fff' : '#ABB2C0' } width={ 10 } height={ 10 }/>
            </div>
        </button>
    );
}

Label.defaultProps = {
    border : 'border',
    color : 'text-gray-500',
    fontWeight: 'font-light',
    display : 'relative',
    rounded : 'rounded-full',
    padding : 'py-2 px-5 pl-12',
};