import React from 'react';
import classNames from 'classnames';

export default function Input({ className, background, color, rounded, padding, ...props }){
    return (
        <input
            className={ classNames(
                className,
                background,
                color,
                rounded,
                padding,
            ) }
            { ...props }
        />
    );
}

Input.defaultProps = {
    type : 'text',
    background : 'bg-gray-100',
    color : 'text-gray-500',
    rounded : 'rounded-sm',
    padding : 'px-4 py-2'
};