import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import styles from './index.module.scss';

export default function SwitchButton({ value, className, ...props }){
    return (
        <div
            className={ classNames(
                className,
                styles.container,
                { [styles.notActive] : !value }
            ) }
            { ...props }
        >
            <span className={ classNames(styles.toggle) }/>
        </div>
    );
}

SwitchButton.defaultProps = {

};
SwitchButton.propTypes = {
    value : PropTypes.bool.isRequired,
};