import React from 'react';
import classNames from 'classnames';

export default function TextArea({ background, className, color, padding, rounded, ...props }){
    return (
        <textarea
            className={ classNames(className, background, color, padding, rounded) }
            { ...props }
        />
    );
}

TextArea.defaultProps = {
    color : 'text-gray-500 ',
    padding : 'px-4 py-2',
    rounded : 'rounded-sm',
    background : 'bg-gray-100'
};