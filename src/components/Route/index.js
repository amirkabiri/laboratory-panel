import React, { useEffect } from 'react';
import { Route as OriginRoute } from 'react-router-dom';

function addClasses(el, classes){
    if(typeof classes !== 'string' || classes.trim().length === 0) return;
    const classNames = classes.split(' ');
    const { classList } = el;

    for(let className of classNames){
        if(!classList.contains(className)) classList.add(className);
    }
}
function removeClasses(el, classes){
    if(typeof classes !== 'string' || classes.trim().length === 0) return;
    const classNames = classes.split(' ');
    const { classList } = el;

    for(let className of classNames){
        if(classList.contains(className)) classList.remove(className);
    }
}

export default function Route({ bodyClassName, rootClassName, ...route }) {
    useEffect(() => {
        addClasses(document.body, bodyClassName);
        addClasses(document.getElementById('root'), rootClassName);

        return () => {
            removeClasses(document.body, bodyClassName);
            removeClasses(document.getElementById('root'), rootClassName);
        };
    }, []);

    return (
        <OriginRoute
            { ...route }
        />
    );
}

Route.defaultProps = {
    exact : true
};