import React from 'react';
import classNames from 'classnames';

export default function FormGroup({ flex, label, className, children, ...props }){
    return (
        <label
            className={ classNames(className, flex) }
            { ...props }
        >
            <span className="text-sm font-light mb-2 text-gray-300">{ label }</span>
            { children }
        </label>
    );
}

FormGroup.defaultProps = {
    flex : 'flex flex-col'
};