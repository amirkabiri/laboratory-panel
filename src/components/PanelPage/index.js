import React, {useEffect, useState} from 'react';
import SideBar from "./SideBar";
import styles from './index.module.scss';
import Header from "./Header";
import classNames from 'classnames';
import PropTypes from 'prop-types';

export default function PanelPage({ breadcrumb, title, description, meta, children, className, ...props }){
    const [contentStyle, setContentStyle] = useState({
        transform : 'translateY(100px)',
        opacity : 0,
    });

    useEffect(() => {
        setContentStyle({
            transform : 'translateY(0)',
            opacity : 1,
            transition : 'all .3s',
        });
    }, []);

    return (
        <div
            className={ classNames(styles.container, className) }
            { ...props }
        >
            <SideBar/>

            <div className={ classNames(styles.main, 'py-6 md:py-16') }>
                <div className="w-full container mx-auto px-5 md:px-10 lg:px-20">
                    <Header breadcrumb={ breadcrumb }/>

                    <div className="flex items-start justify-between mt-10 mb-8">
                        <div>
                            <h3 className="text-gray-500 text-xl">{ title }</h3>
                            { description && (<p className="text-gray-300 mt-1 text-sm font-light">{ description }</p>) }
                        </div>
                        { meta && (<div className="text-gray-500 text-lg whitespace-no-wrap">{ meta }</div>) }
                    </div>

                    <div style={ contentStyle }>
                        { children }
                    </div>

                    <br/><br/>
                </div>
            </div>
        </div>
    );
}

PanelPage.propTypes = {
    title : PropTypes.string.isRequired,
    meta : PropTypes.string,
    description : PropTypes.string
};