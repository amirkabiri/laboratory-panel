import React from 'react';
import styles from "./index.module.scss";
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import AngleDown from "icons/AngleDown";
import PropTypes from 'prop-types';

export default function Header({ breadcrumb }){
    return (
        <div className="flex items-center justify-between">
            <div className={ classNames('flex items-center', styles.breadcrumb) }>
                {
                    [{ title : 'خانه', href : '/' }]
                        .concat(breadcrumb)
                        .map((item, index) => (
                        <Link
                            key={ index }
                            to={ item.href }
                            className={ classNames(
                                { 'text-blue-500' : index === breadcrumb.length },
                                { 'text-gray-300' : index !== breadcrumb.length },
                            ) }
                        >{ item.title }</Link>
                    ))
                }
            </div>
            <div className="flex items-center justify-center">
                <span className="text-gray-500 ml-2">ladansh90@gmail.com</span>

                <button className="bg-white border border-gray-200 rounded-sm p-1">
                    <AngleDown/>
                </button>
            </div>
        </div>
    );
}

Header.defaultProps = {
    breadcrumb: []
};
Header.propTypes = {
    breadcrumb: PropTypes.array
};