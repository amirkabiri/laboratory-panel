import React, { useEffect, useState } from 'react';
import styles from './index.module.scss';
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';
import { Link } from "react-router-dom";
import items from 'shared/menu';

export default function SideBar(){
    const [active, setActive] = useState(-1);
    const pathname = useHistory().location.pathname.replace(/^\/*/, '').replace(/\/*$/, '');

    const onClick = index => () => setActive(index);

    useEffect(() => {
        for(let i = 0; i < items.length; i ++){
            const item = items[i];

            if(item.regex === undefined || !item.regex.test(pathname)) continue;

            setActive(i);
            break;
        }
    }, []);

    return (
        <div className={ styles.container }>
            {
                items.map(({ icon: Icon, href }, index) => (
                    <Link
                        to={ href }
                        className={ classNames(
                            styles.item,
                            { [styles.active] : index === active }
                        )}
                        key={ index }
                        onClick={ onClick(index) }
                    >
                        <Icon/>
                    </Link>
                ))
            }
        </div>
    );
}