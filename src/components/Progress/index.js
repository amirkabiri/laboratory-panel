import React from 'react';
import classNames from 'classnames';

export default function Progress({ background, overflow, rounded, width, height, percent, className, ...props }){
    return (
        <div
            className={ classNames(
                overflow,
                rounded,
                width,
                'bg-gray-100',
                className
            ) }
            style={{ height }}
            { ...props }
        >
            <div className={ classNames(
                background,
                rounded,
            ) } style={{ height, width : percent + '%' }}/>
        </div>
    );
}

Progress.defaultProps = {
    percent : 20,
    width : 'w-full',
    height : 10,
    overflow : 'overflow-hidden',
    rounded : 'rounded-full',
    background : 'bg-orange-300'
};