import React, {useEffect, useState} from 'react';
import styles from './index.module.scss';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const fileInput = document.createElement('input');
fileInput.type = 'file';

function readFileAsDataURL(file){
    return new Promise((resolve, reject) => {
        const fileReader = new FileReader();
        fileReader.onload = () => resolve(fileReader.result);
        fileReader.onerror = err => reject(err);
        fileReader.readAsDataURL(file);
    });
}

export default function UploadBox({ className, title, onChange, value, ...props }){
    const [highlight, setHighlight] = useState(false);
    const [preview, setPreview] = useState(value);

    const onDrop = e => {
        e.preventDefault();

        handleFiles(e.dataTransfer.files);
        setHighlight(false);
    };
    const handleFiles = async files => {
        if(!files.length) return;
        const [file] = files;

        const dataURL = await readFileAsDataURL(file);
        setPreview(dataURL);
        onChange(file);
    };

    useEffect(() => {
        fileInput.onchange = () => handleFiles(fileInput.files);

        return () => {
            fileInput.onchange = null;
        };
    }, []);

    return (
        <div
            className={ classNames(
                className,
                { [styles.highlight] : highlight },
                styles.container,
                "bg-gray-100 rounded-sm p-6"
            ) }
            { ...props }
        >

            { preview && ( <img className={ styles.preview } src={ preview } alt="preview of uploaded photo"/> ) }
            <div className={ styles.half }>
                <span className={ classNames(styles.icon, "text-gray-500")}>+</span>
                <span className={ classNames(styles.title, "text-gray-500 font-light") }>{ title }</span>
            </div>
            <div
                className="z-50 absolute w-full h-full right-0 left-0 top-0 bottom-0"
                onDragOver={ e => e.preventDefault() }
                onDragEnter={ () => setHighlight(true) }
                onDragLeave={ () => setHighlight(false) }
                onDrop={ onDrop }
                onClick={ () => fileInput.click() }
            />
        </div>
    );
}

UploadBox.defaultProps = {
    onChange : () => null
};
UploadBox.propTypes = {
    title : PropTypes.string.isRequired
};