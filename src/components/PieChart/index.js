import React, {useEffect, useRef} from 'react';
import classNames from "classnames";
import styles from './index.module.scss';
import PropTypes from "prop-types";

export const colors = ['#FEF2CB', '#CBEFFE', '#D1F5E1', '#E1DAFB'];

export default function PieChart({ className, percents, size, lineWidth, children, colorSet=colors, ...props }){
    const canvasRef = useRef();

    useEffect(() => {
        if(!canvasRef.current) return;
        const { current: canvas } = canvasRef;
        const ctx = canvas.getContext('2d');
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        ctx.save();
        ctx.translate(canvas.width / 2, canvas.height / 2);
        ctx.rotate(-90 * Math.PI / 180);
        ctx.lineWidth = lineWidth;

        let cumulativePercent = 0;
        for(let i = 0; i < percents.length; i ++){
            const percent = percents[i];

            ctx.beginPath();
            ctx.strokeStyle = colorSet[i % colorSet.length];
            ctx.arc(
                0,
                0,
                canvas.width/2 - lineWidth/2,
                (cumulativePercent * 2 * Math.PI) / 100,
                ((cumulativePercent + percent) * 2 * Math.PI) / 100
            );
            ctx.stroke();
            ctx.closePath();

            cumulativePercent += percent;
        }

        ctx.restore();
    }, [percents]);

    return (
        <div
            className={ classNames(className, styles.container) }
            { ...props }
        >
            <canvas
                className={ styles.canvas }
                ref={ canvasRef }
                width={ size }
                height={ size }
            />
            <div
                className={ styles.children }
                style={{
                    width : size - 2 * lineWidth,
                    height : size - 2 * lineWidth,
                }}
            >{ children }</div>
        </div>
    );
}

PieChart.defaultProps = {
    size : 120,
    lineWidth : 24
};
PieChart.propTypes = {
    percents : PropTypes.array.isRequired,
    size : PropTypes.number,
    lineWidth : PropTypes.number,
};