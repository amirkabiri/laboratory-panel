import React from 'react';
import classNames from 'classnames';
import Link from "icons/Link";

export default function LinkInput({ className, color, rounded, padding, style, background, flex, overflow, ...props }){
    return (
        <div
            className={ classNames(
                className,
                background,
                flex,
                overflow,
                rounded
            ) }
            style={ style }
        >
            <input
                className={ classNames(color, padding, 'flex-1', background) }
                { ...props }
            />
            <Link className="py-1 w-12"/>
        </div>
    );
}

LinkInput.defaultProps = {
    background : 'bg-gray-100',
    color : 'text-gray-500',
    rounded : 'rounded-sm',
    flex : 'flex items-center',
    padding : 'pr-4 py-2',
    dir : 'ltr',
    overflow : 'overflow-hidden'
};