import React from 'react';
import { Bar } from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';

defaults.global.defaultFontFamily = "iransans";

export default function BarChart({ data, xLabels }){
    const state = {
        labels: xLabels,
        datasets: [
            {
                label: 'Rainfall',
                fill: false,
                lineTension: 0,
                backgroundColor: '#204BA0',
                borderWidth: 2,
                data: data
            }
        ]
    };
    return (
        <Bar
            data={state}
            options={{
                legend:{
                    display : false
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: true
                        }
                    }],
                    yAxes: [{
                        type: 'linear',
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                        ticks: {
                            max: 100,
                            min: 0,
                            stepSize: 50,
                            callback: function (value) {
                                return (value / this.max * 100).toFixed(0) + '%';
                            },
                        }
                    }, {
                        type: 'linear',
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',
                        ticks: {
                            max: 80,
                            min: 60,
                            stepSize: 10,
                        }
                    }],
                }
            }}
        />
    );
}