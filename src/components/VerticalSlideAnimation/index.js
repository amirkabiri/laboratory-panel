import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';

export default function VerticalSlideAnimation({ style, className, transition, open, children, ...props }){
    const containerRef = useRef();
    const [height, setHeight] = useState('auto');
    const [maxHeight, setMaxHeight] = useState(0);

    useEffect(() => {
        if(!containerRef.current) return;
        const { height } = containerRef.current.getBoundingClientRect();
        setMaxHeight(height - 9);
    }, []);
    useEffect(() => {
        if(!maxHeight) return;

        setHeight(open ? maxHeight : 0);
    }, [open, maxHeight]);

    return (
        <div
            className={ classNames(
                'transition-all',
                transition,
                className
            ) }
            style={{ height, overflow : 'hidden', ...style }}
            ref={ containerRef }
            { ...props }
        >
            { children }
        </div>
    );
}

VerticalSlideAnimation.defaultProps = {
    transition : 'duration-300'
};