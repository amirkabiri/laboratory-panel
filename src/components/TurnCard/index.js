import React from 'react';
import Card from "../Card";
import Button from "../Button";
import classNames from 'classnames';

export default function TurnCard({ className, ...props }){
    return (
        <Card
            className={ classNames(className, "p-10") }
            { ...props }
        >
            <div className="flex flex-col mb-10 items-stretch md:flex-row md:items-center">
                <div className="flex flex-col items-center flex-1 mb-10 md:mb-0">
                    <strong className="font-medium text-gray-500 text-6xl">302</strong>
                    <span className="text-gray-500 text-xl">نوبت فعلی</span>
                    <p className="text-gray-500 text-lg font-light mb-5">23 نفر منتظر در صف</p>
                    <svg xmlns="http://www.w3.org/2000/svg" width="83.902" height="83.902" viewBox="0 0 83.902 83.902">
                        <g id="Group_2439" transform="translate(-906 -546.099)">
                            <g>
                                <text transform="translate(934 596)" fill="#2b3d60" fontSize="26" fontFamily="SegoeUI, Segoe UI">
                                    <tspan x="15" textAnchor="middle" y="0">۱۵</tspan>
                                </text>
                                <circle cx="39" cy="39" r="39" transform="translate(909 549)" fill="none" stroke="#e9ecf2" strokeMiterlimit="10" strokeWidth="4"/>
                            </g>
                            <path d="M-183.549,79.333A38.951,38.951,0,0,1-144.6,118.284a38.951,38.951,0,0,1-38.951,38.951A38.951,38.951,0,0,1-222.5,118.284" transform="translate(1131.5 469.766)" fill="none" stroke="#12db9d" strokeLinecap="round" strokeMiterlimit="10" strokeWidth="6"/>
                        </g>
                    </svg>
                </div>
                <div className="flex-1">
                    <Button className="mb-4 py-8 text-lg w-full block" color="primary">نوبت بعدی</Button>
                    <Button className="py-8 text-lg w-full block">فراخوان مجدد شماره فعلی</Button>
                </div>
            </div>
            <div className="grid grid-cols-3 grid-rows-1 gap-2">
                <button className="bg-white rounded border text-blue-500 border-blue-500 py-3 hover:bg-blue-500 hover:text-white transition-colors duration-300">حاضر شدن</button>
                <button className="bg-white rounded border text-blue-500 border-blue-500 py-3 hover:bg-blue-500 hover:text-white transition-colors duration-300">غایب شدن</button>
                <button className="bg-white rounded border text-blue-500 border-blue-500 py-3 hover:bg-blue-500 hover:text-white transition-colors duration-300">تکمیل شد</button>
            </div>
        </Card>
    );
}