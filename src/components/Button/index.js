import React from 'react';
import classNames from 'classnames';
import Loading from "icons/Loading";

const colors = {
    success : 'bg-green-500',
    primary : 'bg-blue-500',
    error : 'bg-red-400',
    gray : 'bg-gray-200'
};

export default function Button({ position, loading, hover, transition, rounded, padding, color, className, children, ...props }){
    return (
        <button
            className={ classNames(
                className,
                'text-white',
                hover,
                transition,
                rounded,
                padding,
                position,
                colors[color]
            )}
            disabled={ loading }
            { ...props }
        >
            { children }

            { loading && (
                <div
                    className={ classNames(
                        'absolute right-0 left-0 top-0 bottom-0 w-full h-full flex items-center justify-center',
                        rounded
                    ) }
                    style={{ background : 'rgba(255,255,255,.5)' }}
                >
                    <Loading className="stroke-current text-blue-500" width={ 30 }/>
                </div>
            ) }
        </button>
    );
}
Button.defaultProps = {
    color : 'success',
    hover : 'hover:opacity-75',
    transition : 'transition-opacity duration-300',
    rounded : 'rounded-md',
    padding : 'px-6 py-3',
    position : 'relative'
};
