import React from 'react';

export default function TableHeader({ className, children, ...props }){
    return (
        <thead
            className={ className }
            { ...props }
        >
            { children }
        </thead>
    );
}