import React from 'react';

export default function TableBody({ className, children, ...props }){
    return (
        <tbody
            className={ className }
            { ...props }
        >
            { children }
        </tbody>
    );
}