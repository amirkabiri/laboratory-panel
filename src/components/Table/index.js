import React from 'react';
import classNames from 'classnames';

export default function Table({ children, className, tableClassName, ...props }){
    return (
        <div
            className={ classNames(
                className,
                'overflow-auto table-fixed',
            ) }
            { ...props }
        >
            <table className={ classNames(
                tableClassName
            ) }>
                { children }
            </table>
        </div>
    );
}

Table.defaultProps = {
    tableClassName : 'w-1_5full md:w-full'
};