import React, {useState} from 'react';
import BorderButton from "../BorderButton";
import RecycleBin from "icons/RecycleBin";
import classNames from 'classnames';

export default function RemoveButton({ flex, className, children, ...props }) {
    const [isHover, setIsHover] = useState(false);

    return (
        <BorderButton
            color="error"
            className={ classNames(flex, className) }
            onMouseEnter={ () => setIsHover(true) }
            onMouseLeave={ () => setIsHover(false) }
            transition=""
            { ...props }
        >
            <RecycleBin color={ isHover ? '#fff' : '#fc8181' } className="ml-2"/>
            { children }
        </BorderButton>
    );
}

RemoveButton.defaultProps = {
    flex : 'flex items-center'
};