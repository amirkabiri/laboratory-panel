import React from 'react';
import classNames from 'classnames';
import Loading from "../../icons/Loading";

const colors = {
    error : 'red-400',
    success : 'green-500',
    primary : 'blue-500',
    gray : 'gray-300',
};

export default function BorderButton({ position, loading, color, transition, border, children, hover, rounded, className, padding, ...props }){
    color = colors[color];

    return (
        <button
            className={ classNames(
                className,
                position,
                transition,
                border,
                padding,
                rounded,

                'text-' + color,
                'border-' + color,

                { ['hover:bg-' + color] : hover && !loading, 'hover:text-white' : hover && !loading }
            ) }
            disabled={ loading }
            { ...props }
        >
            { children }

            { loading && (
                <div
                    className={ classNames(
                        'absolute right-0 left-0 top-0 bottom-0 w-full h-full flex items-center justify-center',
                        rounded
                    ) }
                    style={{ background : 'rgba(255,255,255,.5)' }}
                >
                    <Loading className="stroke-current text-blue-500" width={ 30 }/>
                </div>
            ) }
        </button>
    );
}

BorderButton.defaultProps = {
    padding : 'px-6 py-3',
    rounded : 'rounded-md',
    hover : true,
    border : 'border',
    transition : 'transition-colors duration-300',
    position : 'relative',
};