import React from 'react';
import Close from "icons/Close";
import Card from "components/Card";
import styles from './index.module.scss';
import classNames from 'classnames';

export default function Modal({ padding, onClose, open, children }){
    return (
        <div
            className={ classNames(
                padding,
                'px-3 pt-20 pb-10 bg-gray-500 overflow-y-auto',
                styles.container,
                { [styles.open] : open },
            )}
        >
            <button
                className={ classNames('rounded-full p-5', styles.close )}
                onClick={ onClose }
            >
                <Close/>
            </button>
            <div className="flex justify-center items-center min-h-full">
                <Card className="w-full md:w-1/2 xl:w-1/3" padding={ padding }>
                    { children }
                </Card>
            </div>
        </div>
    );
}

Modal.defaultProps = {
    padding : 'p-6 md:p-12'
};
