import React from 'react';
import Button from "./Button";
import AngleRight from "../../icons/AngleRight";
import AngleLeft from "../../icons/AngleLeft";
import ThreeDots from "../../icons/ThreeDots";

export default function Pagination(){
    return (
        <div className="inline-flex items-center justify-center">
            <Button margin="m-3" shadow="shadow-md">
                <AngleRight/>
            </Button>

            <Button>1</Button>
            <Button active={ true }>2</Button>

            <ThreeDots className="m-3"/>

            <Button>9</Button>
            <Button>10</Button>

            <Button margin="m-3" shadow="shadow-md">
                <AngleLeft/>
            </Button>
        </div>
    );
}