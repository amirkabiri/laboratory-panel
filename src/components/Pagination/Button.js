import React from 'react';
import classNames from 'classnames';
import styles from './Button.module.scss';

export default function Button({ shadow, active, margin, border, rounded, text, width, height, display, flex, className, children, ...props }){
    return (
        <button className={ classNames(
            className,
            rounded,
            text,
            width,
            height,
            display,
            flex,
            margin,
            { 'border border-blue-500' : active, [border] : !active },
            shadow,
            styles.button,
            'hover:shadow-xl'
        ) }>
            { children }
        </button>
    );
}

Button.defaultProps = {
    border : 'border border-gray-150',
    rounded : 'rounded',
    text : 'text-gray-300',
    width : 'w-10',
    height : 'h-10',
    display : 'flex',
    flex : 'items-center justify-center',
    margin : 'm-1'
};