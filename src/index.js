import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import Providers from "./providers/Providers";
import 'izitoast/dist/css/iziToast.min.css';

ReactDOM.render(
    <Providers/>,
    document.getElementById('root')
);


serviceWorker.unregister();
