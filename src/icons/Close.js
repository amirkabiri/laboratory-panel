import React from 'react';

export default function Close({ color, ...props }){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="18.201" height="18.201" viewBox="0 0 18.201 18.201" { ...props }>
            <g transform="translate(9.101 -6.979) rotate(45)">
                <path d="M0,0V22.741" transform="translate(11.37 0)" fill="none" stroke={ color } strokeWidth="3"/>
                <path d="M0,0V22.741" transform="translate(0 11.37) rotate(-90)" fill="none" stroke={ color } strokeWidth="3"/>
            </g>
        </svg>
    );
}

Close.defaultProps = {
    color : 'white'
};