import React from 'react';

export default function AngleDown({ color, ...props }){
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="12.728" height="12.727" viewBox="0 0 12.728 12.727" { ...props }>
            <path
                d="M-22074.385-4998.659h-8v8"
                transform="translate(-12073.654 -19137.877) rotate(-135)"
                fill="none"
                stroke={ color }
                strokeWidth="2"
            />
        </svg>
    );
}
AngleDown.defaultProps = {
    color : '#6e7b93'
};