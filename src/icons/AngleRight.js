import React from 'react';

export default function AngleRight({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="12.853" height="12.853" viewBox="0 0 12.853 12.853" { ...props }>
            <path d="M8,8c-.931-.147-8,0-8,0V0" transform="translate(5.767 12.146) rotate(-135)" fill="none"
                  stroke={ color } strokeWidth="2"/>
        </svg>
    );
}

AngleRight.defaultProps = {
    color : '#a8b4c3'
};