import React from 'react';

export default function Escape({color, ...props}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="66.385" height="51.891" viewBox="0 0 66.385 51.891" { ...props }>
            <g transform="translate(2 2)">
                <path
                    d="M36.6,0H74.671a4.912,4.912,0,0,1,4.912,4.912V42.98a4.912,4.912,0,0,1-4.912,4.912H36.6L17.2,24.083Z"
                    transform="translate(-17.197)" fill="none" stroke={ color } strokeLinecap="round"
                    strokeLinejoin="round" strokeWidth="4"/>
                <g transform="translate(37.412 13.232) rotate(45)">
                    <line y2="16.091" transform="translate(8.046 0)" fill="none" stroke={ color } strokeLinecap="round"
                          strokeWidth="4"/>
                    <line x2="16.091" transform="translate(0 8.046)" fill="none" stroke={ color } strokeLinecap="round"
                          strokeWidth="4"/>
                </g>
            </g>
        </svg>
    );
}

Escape.defaultProps = {
    color: '#fff'
};