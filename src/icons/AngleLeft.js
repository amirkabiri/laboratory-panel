import React from 'react';

export default function AngleLeft({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="12.853" height="12.853" viewBox="0 0 12.853 12.853" { ...props }>
            <path d="M8,0C7.069.147,0,0,0,0V8" transform="translate(1.429 6.489) rotate(-45)" fill="none"
                  stroke={ color } strokeWidth="2"/>
        </svg>
    );
}

AngleLeft.defaultProps = {
    color : '#a8b4c3'
};