import React from 'react';

export default function Report({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="25.888" height="25.726" viewBox="0 0 25.888 25.726" { ...props }>
            <g transform="translate(0)">
                <path
                    d="M22.652,25.726h0l-7.512-4.691H1.618A1.62,1.62,0,0,1,0,19.416V1.618A1.62,1.62,0,0,1,1.618,0H24.269a1.62,1.62,0,0,1,1.618,1.618v17.8a1.62,1.62,0,0,1-1.618,1.618H22.652v4.692ZM13.135,14.562V16.18h1.617V14.562Zm.809-9.708a.81.81,0,0,0-.809.81v7.28h1.617V5.664A.81.81,0,0,0,13.945,4.854Z"
                    transform="translate(0 0)" fill={ color }/>
            </g>
        </svg>
    );
}