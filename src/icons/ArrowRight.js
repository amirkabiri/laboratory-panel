import React from 'react';

export default function ArrowRight({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="29.858" height="25.948" viewBox="0 0 29.858 25.948" { ...props }>
            <g transform="translate(-693.5 -314.672)">
                <path d="M0,0H21.219" transform="translate(695.5 327.544)" fill="none" stroke={ color }
                      strokeLinecap="round" strokeWidth="4"/>
                <path d="M14.348,0H0V14.349" transform="translate(720.529 327.647) rotate(135)" fill="none"
                      stroke={ color } strokeLinecap="round" strokeLinejoin="round" strokeWidth="4"/>
            </g>
        </svg>
    );
}

ArrowRight.defaultProps = {
    color : '#abb2c0'
};