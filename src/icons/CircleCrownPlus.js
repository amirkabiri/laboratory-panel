import React from 'react';

export default function CircleCrownPlus({ ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" { ...props }>
            <g transform="translate(-1062 -776)">
                <g transform="translate(1062 776)">
                    <rect width="50" height="50" rx="25" fill="#ffd834"/>
                </g>
                <g transform="translate(1074.2 789.741)">
                    <path
                        d="M25.644,79.8a1.015,1.015,0,0,0-1.1-.141l-5.558,2.706L13.767,76.35a1.016,1.016,0,0,0-1.535,0L7.018,82.363,1.46,79.657A1.016,1.016,0,0,0,.044,80.866L3.9,93.561a1.016,1.016,0,0,0,.972.72h16.25a1.016,1.016,0,0,0,.972-.72l3.859-12.7A1.016,1.016,0,0,0,25.644,79.8Z"
                        transform="translate(0 -76)" fill="#fff"/>
                </g>
                <g transform="translate(0 8)">
                    <circle cx="8" cy="8" r="8" transform="translate(1087 792)" fill="#ffd834"/>
                    <path d="M-5.273-.541V-4.469h-3.9V-6.111h3.9v-3.9h1.661v3.9h3.9v1.642h-3.9V-.541Z"
                          transform="translate(1099.172 805.585)" fill="#fff" stroke="#fff" strokeWidth="1"/>
                </g>
            </g>
        </svg>
    );
}