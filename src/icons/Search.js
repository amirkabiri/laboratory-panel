import React from 'react';

export default function Search({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" { ...props }>
            <path
                d="M10.569,0A10.569,10.569,0,1,0,21.138,10.569,10.581,10.581,0,0,0,10.569,0Zm0,19.187a8.618,8.618,0,1,1,8.618-8.618A8.628,8.628,0,0,1,10.569,19.187Z"
                fill={ color }
            />
            <g transform="translate(16.455 16.455)">
                <path
                    d="M358.3,356.925l-5.593-5.594a.976.976,0,0,0-1.38,1.38l5.594,5.593a.976.976,0,0,0,1.38-1.38Z"
                    transform="translate(-351.046 -351.046)"
                    fill={ color }
                />
            </g>
        </svg>
    );
}

Search.defaultProps = {
    color : "#abb2c0"
};