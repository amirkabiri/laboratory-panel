import React from 'react';

export default function Crown({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="26" height="18.281" viewBox="0 0 26 18.281" { ...props } >
            <path
                d="M25.644,79.8a1.015,1.015,0,0,0-1.1-.141l-5.558,2.706L13.767,76.35a1.016,1.016,0,0,0-1.535,0L7.018,82.363,1.46,79.657A1.016,1.016,0,0,0,.044,80.866L3.9,93.561a1.016,1.016,0,0,0,.972.72h16.25a1.016,1.016,0,0,0,.972-.72l3.859-12.7A1.016,1.016,0,0,0,25.644,79.8Z"
                transform="translate(0 -76)"
                fill={ color }
            />
        </svg>
    );
}

Crown.defaultProps = {
    color : '#ffd834'
};