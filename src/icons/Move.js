import React from 'react';

export default function Move({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="30" viewBox="0 0 12 30" { ...props }>
            <g transform="translate(-159.877)">
                <g transform="translate(159.877)">
                    <path
                        d="M160.581,7.49h10.665a.647.647,0,0,0,.667-.625.6.6,0,0,0-.146-.391L166.435.222a.724.724,0,0,0-.976-.061.681.681,0,0,0-.065.061l-5.332,6.253a.6.6,0,0,0,.1.879A.7.7,0,0,0,160.581,7.49Z"
                        transform="translate(-159.913 0)" fill={ color }/>
                    <path
                        d="M171.21,383.768H160.545a.647.647,0,0,0-.668.624.6.6,0,0,0,.147.393l5.332,6.253a.7.7,0,0,0,.937.1.651.651,0,0,0,.1-.1l5.332-6.253a.6.6,0,0,0-.1-.879A.694.694,0,0,0,171.21,383.768Z"
                        transform="translate(-159.877 -361.272)" fill={ color }/>
                </g>
                <circle cx="4" cy="4" r="4" transform="translate(161.877 11)" fill={ color }/>
            </g>
        </svg>
    );
}

Move.defaultProps = {
    color : '#d4d8df'
};