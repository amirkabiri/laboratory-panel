import React from 'react';

export default function ThreeDots({ ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="23" height="5" viewBox="0 0 23 5" { ...props }>
            <g transform="translate(0.232 -0.463)">
                <circle cx="2.5" cy="2.5" r="2.5" transform="translate(-0.232 0.463)" fill="#ccd5e0"/>
                <circle cx="2.5" cy="2.5" r="2.5" transform="translate(8.768 0.463)" fill="#ccd5e0"/>
                <circle cx="2.5" cy="2.5" r="2.5" transform="translate(17.768 0.463)" fill="#ccd5e0"/>
            </g>
        </svg>
    );
}