import React from 'react';

export default function Export({ color, ...props }) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="24.373" height="24.373" viewBox="0 0 24.373 24.373" { ...props }>
            <path
                d="M222.678,16.753l9.885-9.886,1.978,1.978a.937.937,0,0,0,.977.218.948.948,0,0,0,.341-.218.921.921,0,0,0,.268-.576l.66-7.25A.935.935,0,0,0,235.774.006l-7.25.66a.933.933,0,0,0-.576,1.588l1.978,1.978-9.885,9.884a.933.933,0,0,0,0,1.318l1.318,1.318A.933.933,0,0,0,222.678,16.753Z"
                transform="translate(-212.418 -0.002)"
                fill={ color }
            />
            <path
                d="M1.5,24.067H22.563a1.5,1.5,0,0,0,1.5-1.5V14.54H21.058v6.518H3.008V3.008H9.526V0H1.5A1.5,1.5,0,0,0,0,1.5V22.563A1.5,1.5,0,0,0,1.5,24.067Z"
                transform="translate(0 0.306)"
                fill={ color }
            />
        </svg>
    );
}

Export.defaultProps = {
    color : '#d4d8df'
};
