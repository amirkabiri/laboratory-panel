import React from 'react';

export default function RecycleBin({color, ...props}) {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="20.476" height="22.767" viewBox="0 0 20.476 22.767" {...props}>
            <g transform="translate(0.25 0.25)">
                <path
                    d="M76.206,114.909H61.6a.577.577,0,0,0-.433.144.491.491,0,0,0-.144.433l1.5,13.077a2.858,2.858,0,0,0,2.858,2.511h7.275a2.858,2.858,0,0,0,2.887-2.6l1.241-13.048a.491.491,0,0,0-.144-.375A.577.577,0,0,0,76.206,114.909Z"
                    transform="translate(-58.915 -108.557)"
                    fill={ color }
                />
                <path
                    d="M42.392,2.6H36.445V1.674A1.617,1.617,0,0,0,34.888,0H31.162a1.617,1.617,0,0,0-1.646,1.586q0,.044,0,.088V2.6H23.57a.577.577,0,1,0,0,1.155H42.392a.577.577,0,1,0,0-1.155Z"
                    transform="translate(-22.993 0)"
                    fill={ color }
                    stroke={ color }
                    strokeWidth="0.5"
                />
            </g>
        </svg>
    );
}

RecycleBin.defaultProps = {
    color : '#D4D8DF'
};