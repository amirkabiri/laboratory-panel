import React, { useState, createContext } from 'react';

export const initialState = {
    open : false,
    content : null,
    props : {}
};

export const ModalContext = createContext();

export function ModalProvider({ children }){
    const [modal, setModal] = useState(initialState);

    const isOpen = modal.open;
    const open = (content, props = {}) => setModal(modal => ({
        ...modal,
        open : true,
        content,
        props
    }));
    const close = () => setModal(modal => ({
        ...modal,
        open : false,
    }));

    return (
        <ModalContext.Provider value={{ modal, setModal, isOpen, open, close }}>
            { children }
        </ModalContext.Provider>
    );
}