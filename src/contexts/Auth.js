import React, { useState, createContext, useEffect } from 'react';
import tokenChangeEvent from 'events/tokenChange';

export const initialState = {
    token : null,
    user : {}
};

export const AuthContext = createContext();

export function AuthProvider({ children }){
    const [auth, setAuth] = useState(initialState);

    const isLoggedIn = () => !!auth.token;
    const logout = () => setAuth(initialState);
    const setUser = user => setAuth(auth => ({
        ...auth,
        user
    }));
    const setToken = token => setAuth(auth => ({
        ...auth,
        token
    }));

    useEffect(() => {
        tokenChangeEvent.fire(auth.token);
    }, [auth.token]);

    return (
        <AuthContext.Provider value={{ auth, setAuth, isLoggedIn, logout, setUser, setToken }}>
            { children }
        </AuthContext.Provider>
    );
}