export const API_URL = (
    process.env.REACT_APP_API_URL || 'http://api2.barsam.co/api/'
).replace(/\/*$/, '') + '/';

export const API_PROXY_URL = (
    process.env.REACT_APP_API_PROXY_URL || 'https://cors-anywhere.herokuapp.com/'
).replace(/\/*$/, '') + '/';

export const API_PROXY_ENABLE = process.env.REACT_APP_API_PROXY_ENABLE || false;

export const API_FULL_URL = (API_PROXY_ENABLE ? API_PROXY_URL : '') + API_URL;