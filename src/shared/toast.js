import iziToast from 'izitoast';

iziToast.settings({
    timeout: 10000,
    resetOnHover: true,
    icon: 'material-icons',
    rtl : true
});

export default iziToast;