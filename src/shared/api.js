import axios from 'axios';
import { API_FULL_URL } from "./config";
import toast from 'shared/toast';

export const api = axios.create({
    baseURL: API_FULL_URL,
    timeout: 0,
    responseType: 'json',
    responseEncoding: 'utf8',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'X-Requested-With' : 'http://localhost:3000',
    }
});

api.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
}, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

api.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
}, function (error) {
    if (error.response) {
        const { status } = error.response;
        if(status >= 500){
            toast.error({
                title : 'خطای سرور',
                message : 'مشکلی در سمت سرور به وجود آمده است'
            });
        }else if (status >= 400 && status < 500){
            toast.error({
                title : '400 range error',
                message : 'check console'
            });
        }
    } else if (error.request) {
        if(!navigator.onLine){
            toast.error({
                title : 'عدم اتصال به اینترنت',
                message : 'اتصال خود را بررسی کرده و دوباره امتحان کنید'
            });
        }else{
            toast.error({
                title : 'خطای سرور',
                message : 'سرور از دسترس خارج شده است'
            });
        }
    } else {
        toast.error({
            title : 'خطای غیرمنتظره',
            message : 'خطای غیرمنتظره ای رخ داده است'
        });
    }

    return Promise.reject(error);
});


export function apiSuccessOutput({ data }){
    return data;
}