import axios from "axios";
import { useEffect } from "react";

export class AbortHandler{
    constructor() {
        this.controllers = [];

        this.executer = this.executer.bind(this);
        this.cancelToken = this.cancelToken.bind(this);
        this.abort = this.abort.bind(this);
    }

    executer(controller){
        this.controllers.push(controller);
    }

    cancelToken(){
        return new axios.CancelToken(this.executer);
    }

    abort(message = ''){
        console.log('aborted', this.controllers.length)
        for(let controller of this.controllers){
            controller(message);
        }
        this.controllers = [];
    }
}

export function useAbortHandler(cancelOnUnmount= true) {
    const abortHandler = new AbortHandler();

    useEffect(() => {
        return () => cancelOnUnmount && abortHandler.abort();
    }, []);

    return [abortHandler.cancelToken, abortHandler.abort];
}