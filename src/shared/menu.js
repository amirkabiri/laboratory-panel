import Home from "icons/SideBar/Home";
import Profile from "icons/SideBar/Profile";
import Document from "icons/SideBar/Document";
import Graph from "icons/SideBar/Graph";
import Category from "icons/SideBar/Category";
import Lock from "icons/SideBar/Lock";
import Wallet from "icons/SideBar/Wallet";
import Chat from "icons/SideBar/Chat";
import Chart from "icons/SideBar/Chart";

export default [
    {
        icon : Home,
        href : '/',
        regex : /^$/
    },
    {
        icon : Profile,
        href : '/info',
        regex : /^info$/
    },
    {
        icon : Document,
        href : '/experiments',
        regex : /^experiments(\/.*)?$/
    },
    {
        icon : Graph,
        href : '/surveys',
        regex : /^surveys(\/.*)?$/
    },
    {
        icon : Category,
        href : '/sections',
        regex : /^sections(\/.*)?$/
    },
    {
        icon : Lock,
        href : '/users',
        regex : /^users$/
    },
    {
        icon : Wallet,
        href : '/finance',
        regex : /^finance$/
    },
    {
        icon : Chat,
        href : '/customers',
        regex : /^customers$/
    },
    {
        icon : Chart,
        href : '/comments',
        regex : /^comments$/
    },
];