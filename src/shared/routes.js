import Login from "pages/Login";
import Turn from "pages/Turn";
import Info from "pages/Info";
import Users from "pages/Users";
import Experiments from "pages/Experiments";
import Experiment from "pages/Experiment";
import Sections from "pages/Sections";
import Section from "pages/Section";
import Finance from "pages/Finance";
import Comments from "pages/Comments";
import Customers from "pages/Customers";
import Survey from "pages/Survey";
import Surveys from "pages/Surveys";

export default [
    {
        path : '/login',
        component : Login,
        bodyClassName: 'bg-primary-gradient',
    }, {
        path : '/',
        component : Turn,
    }, {
        path : '/info',
        component : Info,
    }, {
        path : '/users',
        component : Users,
    }, {
        path : '/experiments',
        component : Experiments,
    }, {
        path : '/experiments/:id',
        component : Experiment,
    }, {
        path : '/sections',
        component : Sections
    }, {
        path : '/sections/:id',
        component : Section
    }, {
        path : '/finance',
        component : Finance
    }, {
        path : '/comments',
        component : Comments
    }, {
        path : '/customers',
        component : Customers
    }, {
        path : '/surveys/:id',
        component : Survey
    }, {
        path : '/surveys',
        component : Surveys
    }
];