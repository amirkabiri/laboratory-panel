/**
 * Simple implementation of Observer Design Pattern
 */
let events = {};

export function subscribe(key, func){
    if(events[key] === undefined) events[key] = [];

    events[key].push(func);
}
export function unsubscribe(key, func){
    if(events[key] === undefined) events[key] = [];

    events[key] = events[key].filter(observer => observer !== func);
}
export function fire(key, ...args){
    (events[key] || []).forEach(observer => observer(...args));
}

export class Event{
    constructor(key){
        this.key = key;
    }

    subscribe(func){
        return subscribe(this.key, func);
    }

    unsubscribe(func) {
        return unsubscribe(this.key, func);
    }

    fire(...args) {
        return fire(this.key, ...args);
    }
}