import { api, apiSuccessOutput } from "shared/api";
import { useQuery } from "react-query";

export async function getCompanyById(id){
    return apiSuccessOutput(
        await api.get('Company/' + id)
    );
}
export function useCompanyQuery(id){
    return useQuery(['Company', id], (_, id) => getCompanyById(id));
}

export async function updateCompanyById(id, company){
    return apiSuccessOutput(
        await api.put('Company/' + id, company)
    );
}