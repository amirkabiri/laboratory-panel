import { api, apiSuccessOutput } from "shared/api";
import { useMutation, useQuery } from "react-query";

export async function getAllSurveys(options = {}){
    return apiSuccessOutput(
        await api.get('survey', options)
    );
}
export function useSurveysQuery(){
    return useQuery('survey', getAllSurveys);
}


export async function getSurveyById(id, options = {}){
    return apiSuccessOutput(
        await api.get('survey/' + id, options)
    );
}


export async function deleteSurveyById(id, options = {}){
    return apiSuccessOutput(
        await api.delete(`survey/${ id }`, options)
    );
}


export async function updateSurveyById(id, { title, status, questions }, options = {}) {
    return apiSuccessOutput(
        await api.put(`survey/${ id }`, {
            title, status : 1 ,questions
        }, options)
    );
}
export function useUpdateSurveyMutation() {
    return useMutation(updateSurveyById);
}


export async function createSurvey({ title, status, questions }, options = {}){
    return apiSuccessOutput(
        await api.post('survey', {
            title, status, questions
        }, options)
    );
}


export async function getSurveyTotalInfo(id, options = {}){
    return apiSuccessOutput(
        await api.get('Participation/GetTotal/' + id, options)
    );
}
export function useSurveyTotalInfoQuery(id) {
    return useQuery(['Participation/GetTotal', id], (_, id) => getSurveyTotalInfo(id));
}


export async function getSurveyParticipation(id, options = {}) {
    const { questions } = apiSuccessOutput(
        await api.get('Participation/' + id, options)
    );
    return questions;
}
export function useSurveyParticipationQuery(id){
    return useQuery(['Participation', id], (_, id) => getSurveyParticipation(id))
}


export async function getSurveyWeeklyInfo(id, options = {}){
    return apiSuccessOutput(
        await api.get('Participation/GetWeekly', options)
    );
}
export function useSurveyWeeklyInfoQuery(id){
    return useQuery('Participation/GetWeekly', getSurveyWeeklyInfo);
}


export async function updateSurveysOrder(surveys, options = {}){
    return apiSuccessOutput(
        await api.put('survey', {
            List : surveys.map(({ id }, index) => ({
                SurveyId : id,
                order : index
            }))
        }, options)
    );
}