import { api, apiSuccessOutput } from 'shared/api';
import { useQuery } from "react-query";


export async function getCustomers(options = {}) {
    return apiSuccessOutput(
        await api.get('Client', options)
    );
}
export function useCustomersQuery() {
    return useQuery('Client', () => getCustomers());
}


export async function getCustomerById(id, options = {}) {
    return apiSuccessOutput(
        await api.get('Client/' + id, options)
    );
}
export function useCustomerQuery(id){
    return useQuery(['Client', id], (_, id) => getCustomerById(id));
}


export async function deleteCustomerById(id, options = {}) {
    return apiSuccessOutput(
        await api.delete('Client/' + id, options)
    );
}


export async function updateCustomerById(id, { name, family, nationalCode, cellNumber, sex }, options = {}) {
    return apiSuccessOutput(
        await api.put('Client/' + id, { name, family, nationalCode, cellNumber, sex }, options)
    );
}


export async function createCustomer({ name, family, nationalCode, cellNumber, sex }, options = {}) {
    return apiSuccessOutput(
        await api.post('Client', { name, family, nationalCode, cellNumber, sex }, options)
    );
}