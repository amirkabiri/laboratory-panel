import { api, apiSuccessOutput } from 'shared/api';


export async function sendSMS({ to, body }, options){
    return apiSuccessOutput(
        await api.post('SmsNotify', { to, body }, options)
    );
}