import { api, apiSuccessOutput } from "shared/api";
import useSWR from "swr";
import {useMutation, useQuery} from "react-query";

export async function getAllNotes(){
    return apiSuccessOutput(
        await api.get('note')
    );
}

export async function createNote({ title, description, textLink, videoLink, photoLink }){
    return apiSuccessOutput(
        await api.post('note', {
            title, description, textLink, videoLink, photoLink
        })
    );
}

export async function updateNote(id, { title, description, textLink, videoLink, photoLink, order }){
    return apiSuccessOutput(
        await api.put('note/' + id, {
            title, description, textLink, videoLink, photoLink, order
        })
    );
}

export async function deleteNote(id){
    return apiSuccessOutput(
        await api.delete('note/' + id)
    );
}

export async function printNote(noteId){
    return apiSuccessOutput(
        await api.post('PrintRequest', {
            NoteId : noteId
        })
    );
}

export async function getTotalNotesInfo(){
    return apiSuccessOutput(
        await api.get('Statistic/GetTotal')
    );
}
export function useSWRGetTotalNotesInfo(){
    return useSWR('Statistic/GetTotal', getTotalNotesInfo);
}

export async function getNotesSMSStatistics(){
    return apiSuccessOutput(
        await api.get('note/GetSmsChart')
    );
}
export function useNotesSMSStatisticsApi(){
    return useQuery('note/GetSmsChart', getNotesSMSStatistics);
}

export async function updateNotesOrder(notes){
    return apiSuccessOutput(
        await api.put('note', {
            List : notes.map(({ id }, index) => ({
                NoteId : id,
                order : index
            }))
        })
    );
}
export function useUpdateNotesOrderMutation(){
    return useMutation(updateNotesOrder);
}