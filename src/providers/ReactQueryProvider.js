import React from 'react';
import { QueryCache, ReactQueryCacheProvider } from "react-query";
import { ReactQueryDevtools } from "react-query-devtools";


const queryCache = new QueryCache();


export default function ReactQueryProvider({ children }){
    return (
        <ReactQueryCacheProvider queryCache={queryCache}>
            { children }
            {/*<ReactQueryDevtools/>*/}
        </ReactQueryCacheProvider>
    );
}