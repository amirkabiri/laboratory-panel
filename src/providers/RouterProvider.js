import React from 'react';
import { BrowserRouter as Router, Switch } from "react-router-dom";
import routes from 'shared/routes';
import Route from "components/Route";

export default function RouterProvider(){
    return (
        <Router>
            <Switch>
                {
                    routes.map((route, index) => (
                        <Route
                            { ...route }
                            key={ index }
                        />
                    ))
                }
            </Switch>
        </Router>
    );
}