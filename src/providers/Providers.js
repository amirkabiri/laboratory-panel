import React from 'react';

import RouterProvider from "./RouterProvider";
import { AuthProvider as AuthContextProvider } from "contexts/Auth";
import { ModalProvider as ModalContextProvider } from "contexts/Modal";
import ModalProvider from "./ModalProvider";
import ReactQueryProvider from "./ReactQueryProvider";

export default function Providers(){
    return (
        <ReactQueryProvider>
            <AuthContextProvider>
                <ModalContextProvider>
                    <ModalProvider>
                        <RouterProvider/>
                    </ModalProvider>
                </ModalContextProvider>
            </AuthContextProvider>
        </ReactQueryProvider>
    );
}