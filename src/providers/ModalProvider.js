import React, { useContext, useEffect } from 'react';
import { ModalContext } from "contexts/Modal";
import Modal from "components/Modal";

export default function ModalProvider({ children }){
    const { modal, close } = useContext(ModalContext);

    useEffect(() => {
        // hiding body scroll when modal is open
        document.body.style.overflow = modal.open ? 'hidden' : 'auto';

        // closing modal when Scape key pressed
        window.onkeyup = e => e.key === 'Escape' && modal.open && close();

        return () => {
            document.body.style.overflow = 'auto';
            window.onkeyup = null;
        };
    }, [modal.open]);

    return (
        <>
            <div className="relative z-40">
                <Modal
                    onClose={ close }
                    open={ modal.open }
                    { ...modal.props }
                >{ modal.content }</Modal>
            </div>

            <div className="relative z-20">
                { children }
            </div>
        </>
    );
}