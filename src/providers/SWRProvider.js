import React from 'react';
import { SWRConfig } from "swr";
import { api, apiSuccessOutput } from 'shared/api';

const options = {
    fetcher: async (resource, init) => apiSuccessOutput(
        await api.get(resource)
    ),
    // revalidateOnMount,
    revalidateOnFocus : true,
    revalidateOnReconnect : true,
    shouldRetryOnError : true,
    errorRetryInterval : 5000,
    errorRetryCount : 10
};

export default function SWRProvider({ children }){
    return (
        <SWRConfig value={ options }>
            { children }
        </SWRConfig>
    );
}