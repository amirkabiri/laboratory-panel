import { Event } from '../shared/event';

export default new Event('token-change');