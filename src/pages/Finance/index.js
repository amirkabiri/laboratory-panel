import React, { useEffect, useState } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import Badge from "components/Badge";
import Pagination from "components/Pagination";
import TableHeader from "../../components/Table/TableHeader";
import TableBody from "../../components/Table/TableBody";
import Table from "../../components/Table";
import DropDown from "../../components/DropDown";
import AngleDown from "../../icons/AngleDown";
import Funnel from "../../icons/Funnel";
import Export from "../../icons/Export";

export default function Finance() {
    const [data, setData] = useState([]);

    useEffect(() => {
        setData([
            {
                id: '349870',
                status: 'success',
                title: 'آزمایش چکاپ',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
            {
                id: '349870',
                status: 'error',
                title: 'آزمایش ازدواج',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
            {
                id: '349870',
                status: 'success',
                title: 'آزمایش بارداری',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
            {
                id: '349870',
                status: 'success',
                title: 'آزمایش کم خونی',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
            {
                id: '349870',
                status: 'warning',
                title: 'آزمایش گاسترین',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
            {
                id: '349870',
                status: 'success',
                title: 'آزمایش تست تحریک',
                name: 'مهیار جباری',
                cost: '790000',
                payment_id: '34234234',
                payment_date: '1399/02/8'
            },
        ]);
    }, []);

    return (
        <PanelPage
            title="بخش مالی"
            breadcrumb={[
                {title: 'بخش مالی', href: '/finance'},
            ]}
        >
            <Card padding="py-5 md:py-8">
                <div className="px-5 md:px-12 mb-8 flex items-center justify-between">
                    <h3 className="text-gray-500">لیست پرداخت ها</h3>
                    <div className="flex items-center">
                        <span className="text-gray-300 ml-2 font-light">مرتب سازی</span>
                        <DropDown
                            padding="py-2"
                            handle={ props => (
                                <button
                                    className="flex items-center text-gray-300"
                                    { ...props }
                                >
                                    تسویه نشده
                                    <AngleDown className="mr-2"/>
                                </button>
                            ) }
                        >
                            {
                                ['همه','موفق','ناموفق','تسویه نشده'].map((name, index) => (
                                    <button
                                        key={ index }
                                        className="w-full text-gray-500 text-right py-2 px-5 hover:bg-gray-100 transition-colors duration-300"
                                    >{ name }</button>
                                ))
                            }
                        </DropDown>

                        <button className="mr-5 flex items-center text-gray-300 border rounded border-gray-150 py-2 px-4">
                            <Funnel className="ml-2"/>
                            فیلتر
                        </button>

                        <button className="mr-2 flex items-center text-gray-300 border rounded border-gray-150 p-2">
                            <Export/>
                        </button>
                    </div>
                </div>

                <Table tableClassName="w-1_5full md:w-full">
                    <TableHeader className="bg-gray-50 text-sm text-gray-300">
                        <tr>
                            <th className="font-normal py-3">شماره پذیرش</th>
                            <th className="font-normal py-3">وضعیت</th>
                            <th className="font-normal py-3">عنوان آزمایش</th>
                            <th className="font-normal py-3">نام پرداخت کننده</th>
                            <th className="font-normal py-3">مبلغ (ریال)</th>
                            <th className="font-normal py-3">شناسه پرداخت</th>
                            <th className="font-normal py-3">تاریخ پرداخت</th>
                        </tr>
                    </TableHeader>
                    <TableBody>
                        {
                            data.map((row, index) => (
                                <tr
                                    key={index}
                                    className="text-center text-gray-500 border-b border-gray-150"
                                >
                                    <td className="py-5 font-light">#{row.id}</td>
                                    <td className="py-5 font-light">
                                        {row.status === 'success' && (<Badge color="success">موفق</Badge>)}
                                        {row.status === 'error' && (<Badge color="error">ناموفق</Badge>)}
                                        {row.status === 'warning' && (<Badge color="warning">تسویه نشده</Badge>)}
                                    </td>
                                    <td className="py-5 font-light">{row.title}</td>
                                    <td className="py-5 font-light">{row.name}</td>
                                    <td className="py-5 font-light">{row.cost}</td>
                                    <td className="py-5 font-light">{row.payment_id}</td>
                                    <td className="py-5 font-light">{row.payment_date}</td>
                                </tr>
                            ))
                        }
                    </TableBody>
                </Table>

                <div className="pt-5 md:pt-8 flex items-center justify-center">
                    <Pagination/>
                </div>
            </Card>
        </PanelPage>
    )
        ;
}