import React, { useState } from 'react';
import BorderButton from "components/BorderButton";
import { sendSMS } from "resources/sms";
import toast from 'shared/toast';
import { isCancel } from 'axios';

export default function SendSMSButton({ customer }){
    const [loading, setLoading] = useState(false);

    const onSendSMS = async () => {
        try{
            setLoading(true);

            await sendSMS({ to : customer.cellNumber, body : 'اس ام اس تست' });

            toast.success({
                title : 'موفقیت',
                message : `پیامک به شماره ${ customer.cellNumber } ارسال شد`
            });
        }catch (e) {
            if(isCancel(e)) return;

            toast.success({
                title : 'خطا',
                message : `پیامک به شماره ${ customer.cellNumber } ارسال نشد`
            });
        }finally {
            setLoading(false);
        }
    };

    return (
        <BorderButton
            loading={ loading }
            onClick={ onSendSMS }
            color="primary"
            padding="py-2 px-3 text-sm"
        >ارسال پیامک</BorderButton>
    );
}