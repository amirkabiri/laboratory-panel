import React from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import Pagination from "components/Pagination";
import TableHeader from "components/Table/TableHeader";
import TableBody from "components/Table/TableBody";
import Table from "components/Table";
import Export from "icons/Export";
import Button from "components/Button";
import BorderButton from "components/BorderButton";
import SwitchButton from "components/SwitchButton";
import { useCustomersQuery } from "resources/customers";
import SendSMSButton from "./SendSMSButton";

export default function Customers() {
    const { data: customers, isLoading } = useCustomersQuery();

    return (
        <PanelPage
            title="ارتباط با مشتریان"
            breadcrumb={[
                { title: 'ارتباط با مشتریان', href: '/customers' },
            ]}
        >
            <Card padding="py-5 md:py-8 relative z-20" loading={ isLoading }>
                <div className="px-5 md:px-12 mb-8 flex items-center justify-between">
                    <h3 className="text-gray-500">لیست مشتریان</h3>
                    <div className="flex items-center">
                        <button className="flex items-center text-gray-300 border rounded border-gray-150 p-2">
                            <Export/>
                        </button>
                        <Button color="success" padding="py-2 px-3 mr-4">ارسال پیامک گروهی</Button>
                    </div>
                </div>

                <Table tableClassName="w-1_5full md:w-full">
                    <TableHeader className="bg-gray-50 text-sm text-gray-300">
                        <tr>
                            <th className="font-normal py-3">نام</th>
                            <th className="font-normal py-3">نام خانوادگی</th>
                            <th className="font-normal py-3">کد ملی</th>
                            <th className="font-normal py-3">شماره تماس</th>
                            <th className="font-normal py-3"/>
                        </tr>
                    </TableHeader>
                    <TableBody>
                        {
                            (customers || []).map((customer, index) => (
                                <tr
                                    key={ customer.id }
                                    className="text-center text-gray-500 border-b border-gray-150"
                                >
                                    <td className="py-5 font-light">{ customer.name }</td>
                                    <td className="py-5 font-light">{ customer.family }</td>
                                    <td className="py-5 font-light">{ customer.nationalCode }</td>
                                    <td className="py-5 font-light">{ customer.cellNumber }</td>
                                    <td>
                                        <SendSMSButton customer={ customer }/>
                                    </td>
                                </tr>
                            ))
                        }
                    </TableBody>
                </Table>

                <div className="pt-5 md:pt-8 flex items-center justify-center">
                    <Pagination/>
                </div>
            </Card>

            <Card className="p-5 md:p-12 mt-8 relative z-40">
                <h3 className="text-gray-500 text-lg">تنظیمات پیامک</h3>
                <p className="text-gray-500 font-light pt-2">هر مشتری بعد از ثبت شماره تماس خود در صورتی که قبلا در سیستم وجود نداشته باشد پیامک خوش آمد گویی دریافت می‌کند.</p>
                <div className="flex items-center justify-between my-8">
                    <span className="text-gray-500 font-light">پیامک خوش آمد گویی</span>
                    <div className="flex items-center text-gray-500 font-light">
                        فعال
                        <SwitchButton
                            className="mr-5"
                            value={ true }
                        />
                    </div>
                </div>
                <p className="text-gray-500 bg-gray-100 p-5 rounded font-light">
                    ضمن عرض خیر مقدم و خوش آمدگویی به مشتریان محترم،<br/>
                    خواهشمندیم مواردی ایمنی و نکات اشاره شده در بروشور راهنما را به دقت مطالعه فرمایید. <br/>
                    پیشاپیش از همکاری شما بسیار سپاسگزاریم. <br/>
                </p>
            </Card>
        </PanelPage>
    );
}