import React, { useEffect, useState } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import arrayMove from "array-move";
import VotesList from "./VotesList";
import {createSurvey, getAllSurveys, updateSurveysOrder} from "resources/surveys";

export default function Surveys(){
    const [votes, setVotes] = useState([]);
    const [loading, setLoading] = useState(true);

    const fetchVotes = () => {
        setLoading(true);
        getAllSurveys().then(votes => {
            setVotes(votes);
            setLoading(false);
        })
    };
    const createNewSurvey = async () => {
        try{
            setLoading(true);

            await createSurvey({ title : '', questions : [] });

            fetchVotes();
        }catch (e) {
        }finally {
            if(loading) setLoading(false);
        }
    };
    const onSortEnd = ({ oldIndex, newIndex }) => {
        const newVotes = arrayMove(votes, oldIndex, newIndex);

        setVotes(newVotes);

        // TODO : update surveys order
        updateSurveysOrder(newVotes).then(res => console.log('order', res));
    };
    useEffect(() => {
        fetchVotes();
    }, []);

    return (
        <PanelPage
            title="مدیریت نظرسنجی"
            breadcrumb={[{ title : 'مدیریت نظرسنجی', href : '/poll' }]}
        >
            <Card className="p-5 md:p-12" loading={ loading }>
                <VotesList
                    votes={ votes }
                    setVotes={ setVotes }
                    fetchVotes={ fetchVotes }

                    onSortEnd={ onSortEnd }
                    useDragHandle={ true }
                    lockAxis="y"
                />

                <div className="mt-10">
                    <button
                        onClick={ createNewSurvey }
                        className="px-5 py-3 text-gray-500">+ اضافه کردن</button>
                </div>
            </Card>
        </PanelPage>
    );
}