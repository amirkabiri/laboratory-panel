import React from 'react';
import { SortableElement } from 'react-sortable-hoc';
import Vote from "./Vote";

const SortableVote = ({ value }) => (
    <Vote
        { ...value }
    />
);

export default SortableElement(SortableVote);