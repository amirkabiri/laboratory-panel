import React from 'react';
import { SortableContainer } from "react-sortable-hoc";
import SortableVote from "./SortableVote";

function ExperimentList({ votes, ...rest }){
    return (
        <div>
            {
                votes
                    .map((vote, index) => (
                        <SortableVote
                            key={ vote.id }
                            index={ index }
                            value={{
                                number : index + 1,
                                vote,
                                ...rest,
                            }}
                        />
                    ))
            }
        </div>
    );
}

export default SortableContainer(ExperimentList);