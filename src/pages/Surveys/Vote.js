import React, {useCallback} from 'react';
import classNames from 'classnames';
import Move from "icons/Move";
import RecycleBin from "icons/RecycleBin";
import { SortableHandle } from 'react-sortable-hoc';
import Input from "components/Input";
import {deleteSurveyById, updateSurveyById} from "resources/surveys";
import useRemoveModal from "components/RemoveModal";
import toast from "shared/toast";
import BorderButton from "components/BorderButton";
import { useHistory } from 'react-router-dom';
import { debounce } from "lodash/function";

const DragHandle = SortableHandle(() => (
    <button className="mr-3">
        <Move width={ 45 }/>
    </button>
));

export default function Vote({ fetchVotes, vote, number, setVotes, className, ...props }) {
    const { open: openModal, close: closeModal } = useRemoveModal();
    const history = useHistory();

    const updateVote = useCallback(debounce(vote => {
        updateSurveyById(vote.id, vote);
        console.log('update vote', vote);
    }, 1000), []);

    const onChange = ({ target }) => setVotes(votes => votes.map(v => {
        if(vote.id !== v.id) return v;

        const newVote = { ...v, title : target.value };
        updateVote(newVote);

        return newVote;
    }));
    const onDelete = () => openModal({
        title : `آیا از حذف این نظر مطمئن هستید؟`,
        onDelete : async () => {
            setVotes(votes => votes.filter(({ id }) => id !== vote.id));
            closeModal();
            toast.success({
                title : 'حذف شد',
                message : 'نظرسنجی حذف شد'
            });

            try{
                await deleteSurveyById(vote.id);
            }catch (e) {
                setVotes(votes => votes.concat(vote));
            }
        }
    });

    return (
        <div
            className={ classNames(className, 'flex flex-row items-center z-50') }
            { ...props }
        >
            <span className="text-gray-500 font-light w-10">{ number }.</span>
            <div className="flex flex-col md:flex-row flex-1 border-b border-gray-150 py-3">
                <Input
                    placeholder="عنوان آزمایش"
                    className="flex-1 text-gray-500 bg-gray-100 px-5 py-3 rounded mb-2 md:mb-0"
                    value={ vote.title }
                    onChange={ onChange }
                />
                <div className="flex items-center">
                    <BorderButton
                        onClick={ () => history.push('/surveys/' + vote.id) }
                        className="mr-5 text-sm" padding="py-2 px-4" color="primary">
                        جزئیات
                    </BorderButton>
                    <DragHandle/>
                    <button className="mr-3 py-2 px-3" onClick={ onDelete }>
                        <RecycleBin/>
                    </button>
                </div>
            </div>
        </div>
    );
}