import React from 'react';
import PanelPage from "components/PanelPage";
import TurnCard from "components/TurnCard";
import SpecificTurnCard from "components/SpecificTurnCard";
import Card from "components/Card";
import BinaryPieChart from "components/BinaryPieChart";
import PieChartWithInfo from "components/PieChartWithInfo";
import LineChart from "../../components/LineChart";
import BarChart from "../../components/BarChart";

export default function Turn(){
    return (
        <PanelPage
            title="پنل نوبت دهی"
            meta="1:53 ق.ض"
            breadcrumb={[{ title : 'پنل نوبت دهی', href : '/' }]}
        >
            <SpecificTurnCard/>
            <TurnCard className="my-10"/>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 grid-rows-10 w-full gap-3">
                <Card className="flex flex-col items-center justify-around h-56">
                    <h5 className="text-5xl text-gray-500">569</h5>
                    <span className="text-gray-500 text-center text-sm">تعداد مراجعین در روز</span>
                </Card>
                <Card className="flex flex-col items-center justify-around h-56">
                    <BinaryPieChart
                        size={ 100 }
                        percent={ 70 }
                    >
                        <span className="text-gray-500 text-xl">509</span>
                    </BinaryPieChart>
                    <div className="flex items-center justify-around w-full mt-4">
                        <div className="flex items-center text-sm text-gray-500">
                            <span className="w-3 h-3 inline-block rounded-full bg-green-500 ml-2"/>
                            حاضر
                        </div>
                        <div className="flex items-center text-sm text-gray-500">
                            <span className="w-3 h-3 inline-block rounded-full bg-gray-150 ml-2"/>
                            غایب
                        </div>
                    </div>
                </Card>
                <Card className="flex flex-col col-span-1 md:col-start-1 md:col-end-3 lg:col-start-3 lg:col-end-5 h-auto lg:h-56">
                    <span className="mb-1 text-gray-500 text-sm">میانگین زمان انتظار به تفکیک بخش</span>
                    <PieChartWithInfo
                        percents={[10, 30, 40, 20]}
                        items={['نوبت دهی', 'جواب آزمایش', 'پذیرش آزمایشات', 'تسویه هزینه']}
                        metas={['14:30 دقیقه', '14:30 دقیقه', '14:30 دقیقه', '14:30 دقیقه']}
                    />
                </Card>
            </div>

            <div className="mt-3 grid grid-cols-1 grid-rows-2 lg:grid-cols-2 lg:grid-rows-1 gap-3">
                <Card className="flex flex-col h-auto justify-between">
                    <span className="mb-1 text-gray-500 text-sm">تعداد مراجعین هفتگی</span>
                    <LineChart
                        xLabels={["جمعه", "پنجشنبه", "چهارشنبه", "سه شنبه", "دوشنبه", "یکشنبه", "شنبه"]}
                        data={[10, 20, 30, 60, 100, 90, 10]}
                    />
                </Card>
                <Card className="flex flex-col h-auto justify-between">
                    <span className="mb-1 text-gray-500 text-sm">تعداد مراجعین ماهانه</span>
                    <div className="flex flex-col w-full">
                        <LineChart
                            xLabels={ Array(30).fill('') }
                            data={ Array.apply(null,{ length : 30 }).map(() => ~~(Math.random() * 100)) }
                        />
                        <div className="text-center text-gray-500 text-sm">مرداد 1399</div>
                    </div>
                </Card>
            </div>

            <div className="mt-3 grid grid-cols-1 grid-rows-2 lg:grid-cols-2 lg:grid-rows-1 gap-3">
                <Card className="flex flex-col h-auto lg:h-56">
                    <span className="mb-1 text-gray-500 text-sm">تعداد نوبت های گرفته شده</span>
                    <PieChartWithInfo
                        percents={[10, 30, 40, 20]}
                        items={['کیوسک', 'وبسایت', 'پیامک', 'تلفن']}
                        metas={[10, 30, 40, 20]}
                    />
                </Card>
                <Card className="flex flex-col h-auto lg:h-56">
                    <span className="mb-1 text-gray-500 text-sm">میانگین زمان تکمیل درخواست مشتریان به تفکیک بخش</span>
                    <PieChartWithInfo
                        percents={[10, 30, 40, 20]}
                        items={['نوبت دهی', 'جواب آزمایش', 'پذیرش آزمایشات', 'تسویه حساب']}
                        metas={[10, 30, 40, 20]}
                    />
                </Card>
            </div>

            <div className="mt-3 grid grid-cols-1 md:grid-cols-2 gap-4">
                <Card>
                    <h4 className="text-gray-500 font-light mb-4">آنالیز ساعت پیک کاری</h4>
                    <BarChart
                        xLabels={ Array.apply(null,{ length : 13 }).map((_, i) => i + 8).reverse() }
                        data={ Array.apply(null,{ length : 13 }).map((_, i) => ~~(Math.random() * 100)).reverse() }
                    />
                </Card>
            </div>
        </PanelPage>
    );
}