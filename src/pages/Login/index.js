import React from 'react';
import FullLogo from "icons/FullLogo";
import Button from "components/Button";
import FormGroup from 'components/FormGroup';
import Input from "components/Input";
import Card from "components/Card";

export default function Login() {
    return (
        <div className="w-full min-h-screen overflow-y-auto right-0 left-0 bottom-0 top-0 flex justify-center items-center">
            <div className="relative w-full flex justify-center items-center px-2 pt-10 pb-2">
                <div className="flex flex-col items-center w-full md:w-1/2 lg:w-1/3 xl:w-1/4">
                    <FullLogo/>
                    <Card className="mt-10 w-full p-10">
                        <FormGroup label="نام کاربری" className="mb-6">
                            <Input/>
                        </FormGroup>
                        <FormGroup label="کلمه عبور" className="mb-6">
                            <Input/>
                            <a href="#" className="text-sm text-blue-500 mt-2 self-end">فراموشی رمز عبور</a>
                        </FormGroup>
                        <Button className="w-full">ورود به پنل</Button>
                    </Card>
                </div>
            </div>
        </div>
    );
}