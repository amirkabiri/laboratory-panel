import React, {useEffect, useState} from 'react';
import PanelPage from "../../components/PanelPage";
import Card from "../../components/Card";
import Button from "../../components/Button";
import RecycleBin from "../../icons/RecycleBin";
import AngleDown from "../../icons/AngleDown";
import FormGroup from "../../components/FormGroup";
import Input from "../../components/Input";
import useRemoveModal from "../../components/RemoveModal";
import Loading from "../../icons/Loading";

export default function Users(){
    const [users, setUsers] = useState([]);
    const { open: openModal, close: closeModal } = useRemoveModal();

    const onUserDeleteConfirmed = user => () => {
        console.log('user deleted ' , user.name);
        closeModal();
    };
    const onUserDelete = user => () => openModal({
        title : `آیا از حذف  (${ user.name })  مطمئن هستید؟`,
        onDelete : onUserDeleteConfirmed(user)
    });

    useEffect(() => {
        setUsers([
            { name : 'شایان احدی', username : 'shayan_ahadi20', phone : '۰۹۳۶۸۸۰۳۹۲۹' },
            { name : 'شایان احدی', username : 'shayan_ahadi20', phone : '۰۹۳۶۸۸۰۳۹۲۹' },
            { name : 'شایان احدی', username : 'shayan_ahadi20', phone : '۰۹۳۶۸۸۰۳۹۲۹' },
            { name : 'شایان احدی', username : 'shayan_ahadi20', phone : '۰۹۳۶۸۸۰۳۹۲۹' },
            { name : 'شایان احدی', username : 'shayan_ahadi20', phone : '۰۹۳۶۸۸۰۳۹۲۹' },
        ]);
    }, []);

    return (
        <PanelPage
            title="مدیریت کاربران و دسترسی ها"
            description="برای دسترسی به سفارش ها میتوان در این پنل کاربر های مختلف را ایجاد کرد و به هر کدام سطح دسترسی داد تا به قسمت های مختلف دسترسی داشته باشد."
            breadcrumb={[{ title : 'مدیریت کاربران و دسترسی ها', href : '/users' }]}
        >
            <Card className="p-12">
                <h3 className="text-gray-500 font-light text-lg mb-5">لیست افراد</h3>

                <div>
                    {
                        users.map((user, index) => (
                            <div
                                className="flex flex-col md:flex-row items-center justify-between text-gray-500 py-3 border-b border-gray-150"
                                key={ index }
                            >
                                <span>{ user.name }</span>

                                <div className="flex flex-col md:flex-row items-center">
                                    <div className="flex items-center ml-5 mt-3 md:mt-0">
                                        <span className="pl-2 border-l border-gray-150 inline-block">{ user.username }</span>
                                        <span className="pr-2">{ user.phone }</span>
                                    </div>
                                    <div className="flex items-center">
                                        <button
                                            className="p-2"
                                            onClick={ onUserDelete(user, index) }
                                        >
                                            <RecycleBin color="#d4d8df"/>
                                        </button>
                                        <button className="p-2">
                                            <AngleDown color="#ABB2C0"/>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>

                <div className="flex flex-col md:flex-row items-end border-b border-gray-150 py-8">
                    <div className="grid grid-cols-1 grid-rows-3 md:grid-cols-3 md:grid-rows-1 gap-2 w-full flex-1">
                        <FormGroup label="نام و نام خانوادگی" className="mb-2 md:mb-0">
                            <Input/>
                        </FormGroup>
                        <FormGroup label="نام کاربری" className="mb-2 md:mb-0">
                            <Input/>
                        </FormGroup>
                        <FormGroup label="شماره تماس" className="mb-2 md:mb-0">
                            <Input/>
                        </FormGroup>
                    </div>
                    <Button
                        color="success"
                        className="mr-2 h-10 rounded-sm"
                    >+ اضافه</Button>
                </div>

                <div className="py-8">
                    <span className="text-gray-500">+ کاربر جدید</span>
                </div>

                <div className="flex flex-col md:flex-row justify-center mt-5">
                    <Button color="gray" className="w-full md:w-40 md:ml-2">انصراف</Button>
                    <Button color="success" className="w-full md:w-40 md:mr-2 mt-2 md:mt-0">ذخیره اطلاعات</Button>
                </div>
            </Card>
        </PanelPage>
    );
}