import React from 'react';
import { SortableContainer } from "react-sortable-hoc";
import SortableExperiment from "./Experiment";

function ExperimentList({ experiments, ...rest }){
    return (
        <div>
            {
                experiments
                    .map((experiment, index) => (
                        <SortableExperiment
                            key={ experiment.id }
                            index={ index }
                            value={{
                                number : index + 1,
                                experiment : experiment,
                                ...rest
                            }}
                        />
                    ))
            }
        </div>
    );
}

export default SortableContainer(ExperimentList);