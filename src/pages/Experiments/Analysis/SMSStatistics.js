import React from 'react';
import SMS from "icons/SMS";
import PieChartWithInfo from "components/PieChartWithInfo";
import Card from "components/Card";
import { useSWRGetTotalNotesInfo } from "resources/notes";

export default function SMSStatistics() {
    const { data } = useSWRGetTotalNotesInfo();

    const totalView = data ? data.totalViewedCount : 0;
    const totalSMS = data ? data.totalSentSmsCount : 0;
    const totalSMSPercent = (data && data.totalViewedCount !== 0) ? data.totalSentSmsCount * 100 / data.totalViewedCount : 0;

    return (
        <Card className="flex flex-col" loading={ !data }>
            <h4 className="text-gray-500 font-light mb-4 flex items-center">
                <SMS className="ml-3"/>
                نمودار تعداد پیامک از کل مشاهده
            </h4>
            <PieChartWithInfo
                items={['تعداد پیامک', 'کل مشاهده']}
                percents={[totalSMSPercent, 100 - totalSMSPercent]}
                metas={[totalSMS, totalView]}
            />
        </Card>
    );
}