import React from 'react';
import Card from "components/Card";
import PieChartWithHorizontalInfo from "components/PieChartWithHorizontalInfo";
import SMSStatistics from "./SMSStatistics";
import PrintStatistics from "./PrintStatistics";
import SMSChart from "./SMSChart";

export default function Analysis() {
    return (
        <>
            <h3 className="mt-20 mb-5 text-gray-500 text-2xl">آنالیز</h3>
            <div className="grid grid-cols-1 grid-rows-2 md:grid-rows-1 md:grid-cols-2 gap-4">
                <SMSStatistics/>
                <PrintStatistics/>
            </div>
            <div className="mt-4 grid grid-cols-1 grid-rows-2 md:grid-rows-1 md:grid-cols-2 gap-4">
                <SMSChart/>
                <Card className="">
                    <PieChartWithHorizontalInfo
                        size={ 120 }
                        items={['عالی', 'خوب', 'متوسط', 'ضعیف']}
                        percents={[20, 10, 50, 20]}
                        metas={[212, 109, 519, 289]}
                        innerPie={
                            <div className="flex flex-col items-center text-gray-500">
                                <h5 className="text-xl">85%</h5>
                                <span className="text-sm">مثبت</span>
                            </div>
                        }
                    >
                        <div className="flex flex-col items-center text-gray-500 mt-5">
                            <h5 className="text-2xl">1,345</h5>
                            <span className="text-sm">نظرسنجی پاسخ داده شده</span>
                        </div>
                    </PieChartWithHorizontalInfo>
                </Card>
            </div>
        </>
    );
}