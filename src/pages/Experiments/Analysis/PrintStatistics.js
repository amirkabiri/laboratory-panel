import React from 'react';
import PieChartWithInfo from "components/PieChartWithInfo";
import Card from "components/Card";
import { useSWRGetTotalNotesInfo } from "resources/notes";
import Printer from "icons/Printer";

export default function PrintStatistics() {
    const { data } = useSWRGetTotalNotesInfo();

    const totalView = data ? data.totalViewedCount : 0;
    const totalPrint = data ? data.totalPrintedCount : 0;
    const totalPrintPercent = (data && data.totalViewedCount !== 0) ? data.totalPrintedCount * 100 / data.totalViewedCount : 0;

    return (
        <Card className="flex flex-col" loading={ !data }>
            <h4 className="text-gray-500 font-light mb-4 flex items-center">
                <Printer className="ml-3"/>
                نمودار تعداد چاپ از کل مشاهده
            </h4>
            <PieChartWithInfo
                items={['تعداد چاپ', 'کل مشاهده']}
                percents={[totalPrintPercent, 100 - totalPrintPercent]}
                metas={[totalPrint, totalView]}
            />
        </Card>
    );
}