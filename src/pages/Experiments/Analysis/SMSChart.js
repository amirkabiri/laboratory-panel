import React from 'react';
import LineChart from "components/LineChart";
import Card from "components/Card";
import { useNotesSMSStatisticsApi } from "resources/notes";

export default function SMSChart(){
    const { data, isLoading } = useNotesSMSStatisticsApi();

    const labels = (data || []).map(({ dayName }) => dayName);
    const smsCounts = (data || []).map(({ smsCount }) => smsCount);
    const viewCounts = (data || []).map(({ viewCount}) => viewCount);

    return (
        <Card loading={ isLoading }>
            <h4 className="text-gray-500 font-light mb-4">نمودار تعداد پیامک از کل مشاهده</h4>
            <LineChart
                xLabels={ labels }
                smsCounts={ smsCounts }
                viewCounts={ viewCounts }
            />
        </Card>
    );
}