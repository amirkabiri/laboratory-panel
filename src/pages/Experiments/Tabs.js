import React from 'react';
import classNames from "classnames";

export default function Tabs({ className, tabs, active, onChange }){
    return (
        <div className={ classNames(className, 'flex') }>
            {
                tabs.map((tab, index) => (
                    <button
                        key={ tab.key }
                        className={ classNames(
                            'border border-blue-500 flex-1 py-4',
                            { 'rounded-tr-lg rounded-br-lg' : index === 0 },
                            { 'rounded-tl-lg rounded-bl-lg' : index === tabs.length - 1},
                            { 'bg-blue-500 text-white' : active === tab.key },
                            { 'text-blue-500' : active !== tab.key },
                        ) }
                        onClick={ onChange(tab) }
                    >{ tab.title }</button>
                ))
            }
        </div>
    );
}