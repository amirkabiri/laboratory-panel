import React, { useEffect, useState, useCallback } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import Search from "components/Search";
import Tabs from "./Tabs";
import ExperimentList from "./ExperimentsList";
import arrayMove from "array-move";
import Analysis from "./Analysis/index";
import { getAllNotes, useUpdateNotesOrderMutation } from "resources/notes";
import uuid from 'shared/uuid';
import toast from 'shared/toast';

// const tabs = [
//     { title : 'آزمایشات با نسخه', key : 'experiments-with-prescription' },
//     { title : 'آزمایشات مشخص', key : 'specific-experiments' }
// ];

export default function Experiments(){
    const [experiments, setExperiments] = useState([]);
    const [search, setSearch] = useState('');
    const [loading, setLoading] = useState(true);
    // const [activeTab, setActiveTab] = useState(tabs[0].key);
    const [updateNotesOrder] = useUpdateNotesOrderMutation();

    const fetchExperiments = async () => {
        setLoading(true);
        try{
            const notes = await getAllNotes();
            setExperiments(notes);
        }catch (e) {
        }finally {
            setLoading(false);
        }
    }
    const onCreateNewExperiment = () => {
        if(experiments.some(experiment => experiment.temporary)){
            return toast.warning({
                title : 'اخطار',
                message : 'ابتدا آزمایش های موقت موجود را ذخیره یا لغو کنید'
            });
        }

        setExperiments(experiments => experiments.concat({
            id : uuid(),
            title: '',
            description: '',
            textLink: '',
            videoLink: '',
            photoLink: '',
            temporary : true
        }));
    };
    const onSortEnd = async ({ oldIndex, newIndex }) => {
        const newExperiments = arrayMove(experiments, oldIndex, newIndex)
        setExperiments(newExperiments);

        try{
            await updateNotesOrder(newExperiments);
        }catch (e) {
        }
    };

    useEffect(() => {
        fetchExperiments();
    }, []);

    return (
        <PanelPage
            title="مدیریت نحوه انجام آزمایشات"
            description="در این قسمت شما می‌توانید برای هر آزمایش اطلاعات مربوط به نحوه انجام را تعریف کنید و ترتیب آن‌ها را با Drag & Drop تنظیم کنبد"
            breadcrumb={[{ title : 'مدیریت نحوه انجام آزمایشات', href : '/experiments' }]}
        >
            <Card className="p-12" loading={ loading }>
                {/*<Tabs*/}
                {/*    className="mb-10"*/}
                {/*    tabs={ tabs }*/}
                {/*    active={ activeTab }*/}
                {/*    onChange={ ({ key }) => () => setActiveTab(key) }*/}
                {/*/>*/}
                <Search
                    className="mb-8"
                    value={ search }
                    onChange={ ({ target }) => setSearch(target.value) }
                />

                <ExperimentList
                    sorting={ search === '' }
                    fetchExperiments={ fetchExperiments }
                    setExperiments={ setExperiments }
                    experiments={
                        experiments.filter(experiment => new RegExp(search).test(experiment.title))
                    }
                    onSortEnd={ onSortEnd }
                    useDragHandle={ true }
                    lockAxis="y"
                />

                <div className="mt-10">
                    <button
                        className="text-gray-500 p-3"
                        onClick={ onCreateNewExperiment }
                    >+ اضافه کردن</button>
                </div>
            </Card>

            <Analysis/>
        </PanelPage>
    );
}