import React from 'react';
import { SortableElement } from 'react-sortable-hoc';
import Experiment from "./Experiment";

const SortableExperiment = ({ value }) => (
    <Experiment
        { ...value }
    />
);

export default SortableElement(SortableExperiment);