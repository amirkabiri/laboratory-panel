import React, { useCallback, useState } from 'react';
import classNames from 'classnames';
import Move from "icons/Move";
import RecycleBin from "icons/RecycleBin";
import AngleDown from "icons/AngleDown";
import { SortableHandle } from 'react-sortable-hoc';
import Input from "components/Input";
import TextArea from "components/TextArea";
import VerticalSlideAnimation from "components/VerticalSlideAnimation";
import Button from "components/Button";
import { createNote, deleteNote, updateNote } from "resources/notes";
import toast from "shared/toast";
import useRemoveModal from "components/RemoveModal";
import PrintButton from "./PrintButton";
import Badge from "components/Badge";
import { debounce } from 'lodash/function';

const DragHandle = SortableHandle(() => (
    <button className="mr-3">
        <Move width={ 45 }/>
    </button>
));

export default function Experiment({ fetchExperiments, experiment, number, className, sorting, setExperiments, ...props }) {
    const [open, setOpen] = useState(false);
    const [temporaryLoading, setTemporaryLoading] = useState(false);
    const { open: openModal, close: closeModal } = useRemoveModal();

    const updateExperiment = useCallback(debounce(exp => updateNote(experiment.id, exp), 1000), []);
    const onTemporarySave = async () => {
        setTemporaryLoading(true);
        try{
            await createNote(experiment);
            fetchExperiments();
        }catch (e) {
        }finally {
            setTemporaryLoading(false);
        }
    };
    const onTemporaryCancel = () => setExperiments(experiments => experiments.filter(({ id }) => experiment.id !== id));
    const onChange = key => ({ target }) => setExperiments(experiments => experiments.map(exp => {
        if(experiment.id !== exp.id) return exp;

        const newExperiment = { ...exp, [key] : target.value };

        if(!newExperiment.temporary) updateExperiment(newExperiment);

        return newExperiment;
    }));
    const onDelete = () => openModal({
        title : `آیا از حذف  (${ experiment.title })  مطمئنی هستید؟`,
        onDelete : onDeleteConfirmed
    });
    const onDeleteConfirmed = async () => {
        setExperiments(experiments => experiments.filter(({ id }) => id !== experiment.id));
        closeModal();
        toast.success({
            title : 'حذف شد',
            message : 'آزمایش حذف شد'
        });

        try{
            await deleteNote(experiment.id);
        }catch (e) {
            setExperiments(experiments => experiments.concat(experiment));
        }
    };

    return (
        <div
            className={ classNames(className, 'flex flex-row z-50') }
            { ...props }
        >
            <span className="text-gray-500 font-light w-10 pt-3">{ number }.</span>
            <div className="flex flex-col flex-1 border-b border-gray-150 mb-3 pb-3">
                <div className="flex flex-col md:flex-row">
                    <Input
                        placeholder="عنوان آزمایش"
                        className="flex-1 text-gray-500 bg-gray-100 px-5 py-3 rounded mb-2 md:mb-0"
                        value={ experiment.title }
                        onChange={ onChange('title') }
                    />
                    <div className="flex items-center">
                        { !experiment.temporary && (
                            <>
                                { sorting && ( <DragHandle/> ) }
                                <button className="mr-3 py-2 px-3" onClick={ onDelete }>
                                    <RecycleBin/>
                                </button>
                                <PrintButton noteId={ experiment.id }/>
                                <button
                                    className="mr-3 py-2 px-3"
                                    onClick={ () => setOpen(open => !open) }
                                >
                                    <AngleDown
                                        style={{ transform : `rotate(${ open ? 180 : 0 }deg)`, transition : 'transform .3s' }}
                                        color="#ABB2C0"/>
                                </button>
                            </>
                        )}
                        { experiment.temporary && (
                            <Badge className="mr-3" padding="py-3 px-5" color="error">موقت</Badge>
                        )}
                    </div>
                </div>
                <VerticalSlideAnimation open={ experiment.temporary || open } className="flex flex-col">
                    <TextArea
                        className="bg-gray-100 w-full my-3 h-32"
                        placeholder="توضیحات آزمایش"
                        rounded="rounded"
                        value={ experiment.description }
                        onChange={ onChange('description') }
                    />
                    <div className="grid grid-cols-3 gap-3">
                        <Input
                            placeholder="لینک متن"
                            rounded="rounded"
                            value={ experiment.textLink }
                            onChange={ onChange('textLink') }
                        />
                        <Input
                            placeholder="لینک ویدیو"
                            rounded="rounded"
                            value={ experiment.videoLink }
                            onChange={ onChange('videoLink') }
                        />
                        <Input
                            placeholder="لینک عکس"
                            rounded="rounded"
                            value={ experiment.photoLink }
                            onChange={ onChange('photoLink') }
                        />
                    </div>
                    { experiment.temporary && (
                        <div className="grid grid-cols-2 gap-3 mt-3">
                            <Button
                                onClick={ onTemporarySave }
                                padding="py-2"
                                color="success"
                                loading={ temporaryLoading }
                            >ذخیره</Button>
                            <Button
                                onClick={ onTemporaryCancel }
                                padding="py-2"
                                color="gray"
                            >انصراف</Button>
                        </div>
                    )}
                </VerticalSlideAnimation>
            </div>
        </div>
    );
}