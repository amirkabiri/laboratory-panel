import React, { useState } from 'react';
import toast from 'shared/toast';
import { printNote } from "resources/notes";
import BorderButton from "components/BorderButton";

export default function PrintButton({ noteId }){
    const [loading, setLoading] = useState(false);

    const onClick = async () => {
        setLoading(true);

        try{
            await printNote(noteId);
            toast.success({
                title : 'انجام شد',
                message : 'پرینت با موفقیت انجام شد'
            });
        }catch (e) {

        }finally {
            setLoading(false);
        }
    };

    return (
        <BorderButton
            loading={ loading }
            onClick={ onClick }
            className="mr-3"
            color="primary"
            rounded="rounded"
            padding="py-2 px-5"
        >چاپ</BorderButton>
    );
}