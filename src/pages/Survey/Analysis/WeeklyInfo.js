import React from 'react';
import Progress from "components/Progress";
import MixedChart from "./MixedChart";
import Card from "components/Card";
import { useSurveyWeeklyInfoQuery } from "resources/surveys";

function TodayAndYesterday({ today, yesterday }){
    return (
        <>
            {
                today && (
                    <div className="flex items-start my-10">
                        <span className="w-20 text-gray-500">امروز</span>
                        <div className="flex flex-col items-center flex-1">
                            <Progress background="bg-teal-300" height={ 16 } percent={ Math.abs(today.growthPercent) }/>
                            <span className="text-green-500 mt-3">{ Math.abs(today.growthPercent) }%</span>
                            <span className="text-gray-500 font-light">{ today.growthPercent >= 0 ? 'افزایش' : 'کاهش' } نسبت به روز قبل</span>
                        </div>
                    </div>
                )
            }
            {
                yesterday && (
                    <div className="flex items-start">
                        <span className="w-20 text-gray-500">دیروز</span>
                        <div className="flex flex-col items-center flex-1">
                            <Progress background="bg-green-300" height={ 16 } percent={ Math.abs(yesterday.growthPercent) }/>
                            <span className="text-gray-500 mt-3">{ yesterday.participateCount }</span>
                            <span className="text-gray-500 font-light">نظرسنجی پاسخ داده شده</span>
                        </div>
                    </div>
                )
            }
        </>
    );
}

export default function WeeklyInfo() {
    let { data, isLoading } = useSurveyWeeklyInfoQuery();

    const daysTitle = data ? data.map(({ day }) => day) : [];
    const daysValue = data ? data.map(({ participateCount }) => participateCount) : [];

    return (
        <Card className="p-5 md:p-12 mt-5 grid grid-cols-1 md:grid-cols-2 gap-5" loading={ isLoading }>
            <div>
                <div className="mb-8 text-gray-500 font-light">نرخ رشد</div>
                <div className="flex items-center">
                    <span className="text-gray-500 text-3xl ml-5">1,345</span>
                    <span className="text-gray-500 font-light">نظرسنجی پاسخ داده شده</span>
                </div>
                <TodayAndYesterday
                    today={ Array.isArray(data) && data.length >= 1 ? data[data.length - 1] : null }
                    yesterday={ Array.isArray(data) && data.length >= 2 ? data[data.length - 2] : null }
                />
            </div>
            <div className="flex flex-col justify-between">
                <div className="mb-8 text-gray-500 font-light">مقایسه زمانی</div>
                <MixedChart
                    xLabels={ daysTitle }
                    data={ daysValue }
                />
                <div className="text-gray-500 font-light text-center">مقایسه نظرات در 7 روز گذشته</div>
            </div>
        </Card>
    );
}