import React from 'react';
import TotalInfo from "./TotalInfo";
import Participation from "./Participation";
import WeeklyInfo from "./WeeklyInfo";

export default function Analysis(){
    return (
        <>
            <h3 className="mt-20 mb-1 text-gray-500 text-2xl">آنالیز و گزارشات</h3>
            <p className="text-gray-300 mb-5 font-light">در این بخش نظرسنجی کاربران آنالیز می‌شوند</p>

            <TotalInfo/>
            <WeeklyInfo/>
            <Participation/>
        </>
    );
}