import React from 'react';
import { Line } from 'react-chartjs-2';
import { defaults } from 'react-chartjs-2';

defaults.global.defaultFontFamily = "iransans";

export default function MixedChart({ xLabels, showXLabels, data }){
    const state = {
        labels: xLabels,
        datasets: [
            {
                label: 'vote',
                fill: false,
                lineTension: 0,
                backgroundColor: '#204BA0',
                borderColor: '#596781',
                borderWidth: 2,
                data: data
            },{
                label: 'نظر',
                fill: false,
                backgroundColor: '#BDF2D5',
                lineTension: 0,
                borderWidth: 2,
                data: data,
                type : 'bar'
            },
        ]
    };
    return (
        <Line
            data={ state }
            options={{
                legend:{
                    display : false
                },
                scales: {
                    xAxes: [{
                        ticks: {
                            display: showXLabels
                        }
                    }],
                    yAxes: [{
                        type: 'linear',
                        display: true,
                        position: 'right',
                        id: 'y-axis-2',
                        ticks: {
                            maxTicksLimit : 5
                        }
                    },{
                        type: 'linear',
                        display: true,
                        position: 'left',
                        id: 'y-axis-1',
                        ticks: {
                            max: 100,
                            min: 0,
                            stepSize: 50,
                            callback: function (value) {
                                return (value / this.max * 100).toFixed(0) + '%';
                            },
                        }
                    }],
                }
            }}
        />
    );
}

MixedChart.defaultProps = {
    showXLabels : true
};