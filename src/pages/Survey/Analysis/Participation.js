import React from 'react';
import Card from "components/Card";
import ProgressGroup from "components/ProgressGroup";
import { useSurveyParticipationQuery } from "resources/surveys";
import { useParams } from 'react-router-dom';

export default function Participation(){
    const { id } = useParams();
    const { data, isLoading } = useSurveyParticipationQuery(id);

    return (
        <Card className="p-5 md:p-12 mt-5" loading={ isLoading }>
            <p className="text-gray-500 font-light mb-10">سوالات تفکیک شده</p>
            <div className="grid grid-cols-1 md:grid-cols-2 gap-x-5 gap-y-10">
                {
                    (data || []).map((question, i) => (
                        <div
                            key={ question.questionId }
                        >
                            <p className="mb-3 text-gray-500">
                                <span className="font-light inline-block w-6">{ i + 1 }.</span>
                                { question.question }
                            </p>
                            <ProgressGroup
                                className="w-full"
                                titles={['عالی', 'خوب', 'متوسط', 'ضعیف']}
                                percents={[question.excellentPercent, question.goodPercent, question.middlePercent, question.poorPercent]}
                                metas={[question.excellentChoice, question.goodChoice, question.middleChoice, question.poorChoice]}
                            />
                        </div>
                    ))
                }
            </div>
        </Card>
    );
}