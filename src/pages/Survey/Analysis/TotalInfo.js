import React from 'react';
import ProgressGroup from "components/ProgressGroup";
import PieChart from "components/PieChart";
import Card from "components/Card";
import { useSurveyTotalInfoQuery } from "resources/surveys";
import { useParams } from 'react-router-dom';

export default function TotalInfo(){
    const { id } = useParams();
    const { data, isLoading } = useSurveyTotalInfoQuery(id);

    const participationCount = data ? data.participationCount : 0;

    const optionsCount = data ? [
        data.excellentChoice,
        data.goodChoice,
        data.middleChoice,
        data.poorChoice,
    ] : [0, 0, 0, 0];
    const optionsPercent = data ? [
        data.excellentPercent,
        data.goodPercent,
        data.middlePercent,
        data.poorPercent,
    ] : [0, 0, 0, 0];

    let statusTitle = 'نامشخص';
    let statusPercent = 0;
    if(data){
        if(data.excellentChoice + data.goodChoice >= data.middleChoice + data.poorChoice){
            statusTitle = 'مثبت';
            statusPercent = data.excellentPercent + data.goodPercent;
        }else{
            statusTitle = 'منفی';
            statusPercent = data.middlePercent + data.poorPercent;
        }
    }

    return (
        <Card className="p-5 md:p-12 flex flex-col-reverse md:flex-row" loading={ isLoading }>
            <div className="w-full md:w-1/2 flex flex-col justify-around">
                <span className="text-gray-500 font-light">وضعیت کلی</span>
                <div className="flex items-center mt-10 mb-4">
                    <span className="text-gray-500 text-3xl ml-5">{ participationCount }</span>
                    <span className="text-gray-500 font-light">نظرسنجی پاسخ داده شده</span>
                </div>
                <ProgressGroup
                    className="w-full"
                    titles={['عالی', 'خوب', 'متوسط', 'ضعیف']}
                    percents={ optionsPercent }
                    metas={ optionsCount }
                />
            </div>
            <div className="w-full md:w-1/2 flex items-center justify-center mb-5 md:mb-0">
                <PieChart
                    percents={ optionsPercent }
                    size={ 200 }
                    lineWidth={ 40 }
                    colorSet={['#BDF2D5', '#D9F4B3', '#FFF2BC', '#FFD5D5']}
                >
                    <div className="flex flex-col items-center">
                        <span className="text-gray-500 text-3xl">{ statusPercent }%</span>
                        <span className="text-gray-500 font-light">{ statusTitle }</span>
                    </div>
                </PieChart>
            </div>
        </Card>
    );
}