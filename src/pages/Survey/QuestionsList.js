import React from 'react';
import { SortableContainer } from "react-sortable-hoc";
import SortableQuestion from "./Question";

function ExperimentList({ questions, ...rest }){
    return (
        <div>
            {
                questions
                    .map((question, index) => (
                        <SortableQuestion
                            key={ index }
                            index={ index }
                            value={{
                                number : index + 1,
                                question,
                                ...rest
                            }}
                        />
                    ))
            }
        </div>
    );
}

export default SortableContainer(ExperimentList);