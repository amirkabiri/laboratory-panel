import React, { useCallback } from 'react';
import classNames from 'classnames';
import Move from "icons/Move";
import RecycleBin from "icons/RecycleBin";
import { SortableHandle } from 'react-sortable-hoc';
import useRemoveModal from "components/RemoveModal";
import Input from "components/Input";
import { updateSurveyById } from "resources/surveys";
import { debounce } from 'lodash/function';

const DragHandle = SortableHandle(() => (
    <button className="mr-3">
        <Move width={ 45 }/>
    </button>
));

export default function Question({ question, number, className, setSurvey, ...props }) {
    const { open: openModal, close: closeModal } = useRemoveModal();

    const debouncedUpdateSurveyById = useCallback(debounce(updateSurveyById, 1000), []);
    const onDelete = () => openModal({
        title : `آیا از حذف سوال (${ question.question }) مطمئن هستید؟`,
        onDelete : () => setSurvey(survey => {
            const newSurvey = {
                ...survey,
                questions : survey.questions.filter(({ id }) => id !== question.id)
            };

            closeModal();
            updateSurveyById(newSurvey.id, newSurvey);

            return newSurvey;
        })
    });
    const onChange = key => ({ target }) => setSurvey(survey => {
        const newSurvey = {
            ...survey,
            questions : survey.questions.map(q => q.id !== question.id ? q : ({ ...q, [key] : target.value }))
        };

        debouncedUpdateSurveyById(newSurvey.id, newSurvey);

        return newSurvey;
    });

    return (
        <div
            className={ classNames(className, 'flex flex-row items-center z-50') }
            { ...props }
        >
            <span className="text-gray-500 font-light w-10">{ number }.</span>
            <div className="flex flex-col md:flex-row flex-1 border-b border-gray-150 py-3">
                <Input
                    placeholder="سوال"
                    className="flex-1 text-gray-500 bg-gray-100 px-5 py-3 rounded mb-2 md:mb-0"
                    value={ question.question }
                    onChange={ onChange('question') }
                />
                <div className="flex items-center">
                    <DragHandle/>
                    <button className="mr-3 py-2 px-3" onClick={ onDelete }>
                        <RecycleBin/>
                    </button>
                </div>
            </div>
        </div>
    );
}