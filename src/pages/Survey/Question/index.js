import React from 'react';
import { SortableElement } from 'react-sortable-hoc';
import Question from "./Question";

const SortableQuestion = ({ value }) => (
    <Question
        { ...value }
    />
);

export default SortableElement(SortableQuestion);