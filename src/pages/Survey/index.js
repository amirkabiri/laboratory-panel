import React, { useEffect, useState } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import arrayMove from "array-move";
import QuestionsList from "./QuestionsList";
import { getSurveyById, updateSurveyById } from "resources/surveys";
import { Link, useParams } from 'react-router-dom';
import Analysis from "./Analysis";
import ArrowRight from "icons/ArrowRight";
import { useAbortHandler } from "shared/abort-handler";
import { isCancel } from 'axios';

const updateQuestionsOrder = survey => ({
    ...survey,
    questions : survey.questions.map((question, index) => ({
        ...question,
        order : index
    }))
});

export default function Survey(){
    const [cancelToken] = useAbortHandler();
    const { id } = useParams();
    const [survey, setSurvey] = useState({});
    const [loading, setLoading] = useState(true);

    const fetchSurvey = async () => {
        if(!loading) setLoading(true);

        try{
            const survey = await getSurveyById(id, { cancelToken : cancelToken() });
            setSurvey(survey);
        }catch (e) {
            // if request cancelled, dont let finally runs
            if(isCancel(e)) return;
        }finally {
            setLoading(false);
        }
    };
    const createNewQuestion = () => setSurvey(survey => {
        const newSurvey = updateQuestionsOrder({
            ...survey,
            questions : survey.questions.concat({
                question : '',
            })
        });

        updateSurveyById(newSurvey.id, newSurvey, { cancelToken : cancelToken() }).then(fetchSurvey);

        return newSurvey;
    });
    const onSortEnd = ({ oldIndex, newIndex }) => setSurvey(survey => {
        const newSurvey = updateQuestionsOrder({
            ...survey,
            questions : arrayMove(survey.questions, oldIndex, newIndex)
        });

        updateSurveyById(newSurvey.id, newSurvey, { cancelToken : cancelToken() });

        return newSurvey;
    });

    useEffect(() => {
        fetchSurvey();
    }, []);

    return (
        <PanelPage
            title="مدیریت نظرسنجی"
            breadcrumb={[{ title : 'مدیریت نظرسنجی', href : '/surveys' }, { title : survey.title, href : '/surveys/' + id }]}
        >
            <Card className="p-5 md:p-12 relative" loading={ loading }>
                <Link to="/surveys" className="z-10 bg-white rounded-full shadow-lg p-4 absolute" style={{ right : -30 }}>
                    <ArrowRight width={ 25 } height={ 25 }/>
                </Link>

                <QuestionsList
                    setSurvey={ setSurvey }

                    questions={ survey.questions || [] }
                    onSortEnd={ onSortEnd }
                    useDragHandle={ true }
                    lockAxis="y"
                />

                <div className="mt-10">
                    <button onClick={ createNewQuestion } className="px-5 py-3 text-gray-500">+ اضافه کردن</button>
                </div>
            </Card>

            <Analysis/>
        </PanelPage>
    );
}