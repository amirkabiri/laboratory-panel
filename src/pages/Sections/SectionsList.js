import React from 'react';
import { SortableContainer } from "react-sortable-hoc";
import SortableSection from "./SortableSection";

function SectionsList({ sections, onTitleChange }){
    return (
        <div className="grid grid-cols-1 md:grid-cols-2 gap-5">
            {
                sections.map((section, index) => (
                    <SortableSection
                        key={ index }
                        index={ index }
                        value={{
                            index,
                            section,
                            onTitleChange : onTitleChange(index),
                            number : index + 1,
                        }}
                    />
                ))
            }
        </div>
    );
}

export default SortableContainer(SectionsList);