import React from 'react';
import Input from "components/Input";
import UploadBox from "components/UploadBox";
import BorderButton from "components/BorderButton";
import classNames from 'classnames';
import { useHistory } from 'react-router-dom';

export default function Section({ className, index, section, onTitleChange, number, ...props }){
    const history = useHistory();

    return (
        <div
            className={ classNames(className, 'z-50 flex border border-gray-150 rounded p-5') }
            { ...props }
        >
            <div className="w-8 text-gray-500">{ number }.</div>
            <div className="flex-1">
                <span className="text-gray-500 block font-light mb-5">نام بخش</span>

                <Input
                    onChange={ onTitleChange }
                    value={ section.title }
                    className="w-full mb-5"
                />

                <div className="flex mb-5">
                    <div className="flex-1 flex flex-col justify-between">
                        <p className="text-sm text-gray-500">برای هر بخش یک عکس و یا آیکون شاخص بارگذاری کنید.</p>
                        <p className="text-sm text-gray-300">فرمت‌های قابل قبول : PNG, JPEG, EPS</p>
                    </div>
                    <UploadBox
                        title=""
                        className="w-20 h-20"
                    />
                </div>

                <BorderButton
                    onClick={ () => history.push('/sections/' + index) }
                    className="block w-full"
                    color="primary"
                >اضافه کردن ایستگاه</BorderButton>
            </div>
        </div>
    );
}