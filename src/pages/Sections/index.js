import React, { useEffect, useState } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import SectionsList from "./SectionsList";
import Button from "../../components/Button";
import arrayMove from "array-move";

export default function Sections(){
    const [sections, setSections] = useState([
        { title : '', icon : null, stations : [] },
        { title : '', icon : null, stations : [] },
        { title : '', icon : null, stations : [] },
        { title : '', icon : null, stations : [] },
    ]);

    const onSectionTitleChange = index => ({ target }) => setSections(sections => sections.map((section, sectionIndex) => {
        if(index !== sectionIndex) return section;

        return {
            ...section,
            title : target.value
        };
    }));

    return (
        <PanelPage
            title="تنظیمات بخش نوبت دهی"
            description="در این قسمت شما می‌توانید بخش‌های مختلف آزمایشگاه را تعریف کنید و ترتیب آن‌ها را با Drag & Drop تنظیم کنید"
            breadcrumb={[{ title : 'تنظیمات بخش نوبت دهی', href : '/sections' }]}
        >
            <Card padding="p-5 md:p-12">
                <SectionsList
                    onTitleChange={ onSectionTitleChange }
                    onSortEnd={
                        ({ oldIndex, newIndex }) => setSections(sections => arrayMove(sections, oldIndex, newIndex))
                    }
                    sections={ sections }
                />

                <div className="py-10">
                    <span className="text-gray-500">+ اضافه کردن</span>
                </div>

                <div className="flex items-center justify-center">
                    <Button color="gray" className="w-40 ml-4">انصراف</Button>
                    <Button color="success" className="w-40">ذخیره اطلاعات</Button>
                </div>
            </Card>
        </PanelPage>
    );
}