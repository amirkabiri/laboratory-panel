import React from 'react';
import Section from "./Section";
import { SortableElement } from "react-sortable-hoc";

const SortableSection = ({ value }) => (
    <Section
        { ...value }
    />
);

export default SortableElement(SortableSection);