import React, {useEffect, useState} from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import useRemoveModal from "components/RemoveModal";
import Button from "components/Button";
import BorderButton from "components/BorderButton";
import RecycleBin from "icons/RecycleBin";
import FormGroup from "components/FormGroup";
import Input from "components/Input";
import TextArea from "components/TextArea";
import UploadBox from "components/UploadBox";
import LinkInput from "components/LinkInput";
import Label from "components/Label";
import ArrowRight from "icons/ArrowRight";
import { Link, useParams } from "react-router-dom";
import RemoveButton from "../../components/RemoveButton";

export default function Experiments({ history }){
    const { open: openModal, close: closeModal } = useRemoveModal();
    const [labels, setLabels] = useState([]);
    const [label, setLabel] = useState('');
    const { id } = useParams();

    const onDelete = () => openModal({
        title : `آیا از حذف  (اسم آیتم)  مطمئنی هستید؟`,
        onDelete : onDeleteConfirmed
    });
    const onDeleteConfirmed = () => {
        console.log('item deleted');
        closeModal();
    };

    return (
        <PanelPage
            title="مدیریت نحوه انجام آزمایشات"
            description="در این قسمت شما می‌توانید برای هر آزمایش اطلاعات مربوط به نحوه انجام را تعریف کنید و ترتیب آن‌ها را با Drag & Drop تنظیم کنبد"
            breadcrumb={[
                { title : 'مدیریت نحوه انجام آزمایشات', href : '/experiments' },
                { title : 'ویرایش راهنما آزمایش', href : '/experiments/' + id },
            ]}
        >
            <Card className="p-12 relative">
                <Link to="/experiments" className="bg-white rounded-full shadow-lg p-4 absolute" style={{ right : -30 }}>
                    <ArrowRight width={ 25 } height={ 25 }/>
                </Link>

                <div className="flex items-center justify-between flex-wrap">
                    <h2 className="text-xl text-gray-500">آزمایش بررسی خون مخفی در مدفوع</h2>
                    <RemoveButton>حذف آیتم</RemoveButton>
                </div>

                <div className="flex flex-col md:flex-row items-center mt-10 mb-5">
                    <FormGroup label="کد آزمایش" className="w-40 ml-0 md:ml-5 w-full md:w-auto">
                        <Input/>
                    </FormGroup>
                    <FormGroup label="لینک صفحه" className="flex-1 w-full mt-5 md:mt-0">
                        <LinkInput/>
                    </FormGroup>
                </div>

                <FormGroup label="توضیحات">
                    <TextArea/>
                </FormGroup>

                <div className="w-full border-b border-gray-150 my-10"/>

                <>
                    <div className="grid grid-cols-1 grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-5">
                        <UploadBox title="عکس راهنما" className="h-56"/>
                        <UploadBox title="عکس راهنما" className="h-56"/>
                    </div>
                    <span className="text-gray-500 mt-10 inline-block">+ اضافه کردن</span>
                </>

                <div className="w-full border-b border-gray-150 my-10"/>

                <>
                    <div className="grid grid-cols-1 grid-rows-2 md:grid-cols-2 md:grid-rows-1 gap-5">
                        <div>
                            <UploadBox title="ویدیو راهنما" className="h-56 mb-4"/>
                            <LinkInput/>
                        </div>
                        <div>
                            <UploadBox title="ویدیو راهنما" className="h-56 mb-4"/>
                            <LinkInput/>
                        </div>
                    </div>
                    <span className="text-gray-500 mt-10 inline-block">+ اضافه کردن</span>
                </>

                <div className="w-full border-b border-gray-150 my-10"/>

                <>
                    <form
                        className="flex flex-col md:flex-row items-end mb-4"
                        onSubmit={ e =>
                            ( e.preventDefault() || label.trim() !== '' ) &&
                            ( setLabels(labels => [...new Set(labels.concat(label))]) || setLabel('') )
                        }
                    >
                        <FormGroup label="اضافه کردن برچسب" className="flex-1 w-full mb-3 md:mb-0">
                            <Input
                                value={ label }
                                onChange={ ({ target }) => setLabel(target.value) }
                            />
                        </FormGroup>
                        <Button
                            type="submit"
                            color="success"
                            className="h-10 mr-3"
                        >+ اضافه</Button>
                    </form>
                    <div>
                        {
                            labels.map((label, index) => (
                                <Label
                                    className="ml-2 mb-2"
                                    key={ index }
                                    onClick={ () => setLabels(labels => labels.filter(l => l !== label)) }
                                >{ label }</Label>
                            ))
                        }
                    </div>
                </>

                <div className="flex justify-center mt-20">
                    <Button color="gray" className="w-48 ml-4">انصراف</Button>
                    <Button color="success" className="w-48">ذخیره اطلاعات</Button>
                </div>
            </Card>
        </PanelPage>
    );
}