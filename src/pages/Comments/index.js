import React, { useEffect, useState } from 'react';
import Card from "components/Card";
import PanelPage from "components/PanelPage";
import DropDown from "components/DropDown";
import AngleDown from "icons/AngleDown";
import Export from "icons/Export";
import Table from "components/Table";
import TableHeader from "components/Table/TableHeader";
import TableBody from "components/Table/TableBody";
import Pagination from "components/Pagination";
import Report from "../../icons/Report";

export default function Comments(){
    const [data, setData] = useState([]);

    useEffect(() => {
        setData([
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
            {firstName : 'مهیار', lastName : 'جباری', reception_id : '342234', phone : '09123456789', score : ~~(Math.random() * 4 + 1), date : '1399/02/08 14:32', report : !!~~(Math.random() * 2)},
        ]);
    }, []);

    return (
        <PanelPage
            title="مشاهده نظرات"
            breadcrumb={[
                {title: 'مشاهده نظرات', href: '/comments'},
            ]}
        >
            <Card padding="py-5 md:py-8">
                <div className="px-5 md:px-12 mb-8 flex items-center justify-between">
                    <h3 className="text-gray-500">لیست نظرات ارسال شده</h3>
                    <div className="flex items-center">
                        <span className="text-gray-300 ml-5 font-light">مرتب سازی</span>
                        <DropDown
                            padding="py-2"
                            handle={ props => (
                                <button
                                    className="flex items-center text-gray-300"
                                    { ...props }
                                >
                                    همه
                                    <AngleDown className="mr-1"/>
                                </button>
                            ) }
                        >
                            {
                                ['همه','موفق','ناموفق','تسویه نشده'].map((name, index) => (
                                    <button
                                        key={ index }
                                        className="w-full text-gray-500 text-right py-2 px-5 hover:bg-gray-100 transition-colors duration-300"
                                    >{ name }</button>
                                ))
                            }
                        </DropDown>

                        <button className="mr-5 text-gray-300 border rounded border-gray-150 p-2">
                            <Export/>
                        </button>
                    </div>
                </div>

                <Table tableClassName="w-1_5full md:w-full">
                    <TableHeader className="bg-gray-50 text-sm text-gray-300">
                        <tr>
                            <th className="font-normal py-3">نام</th>
                            <th className="font-normal py-3">نام خانوادگی</th>
                            <th className="font-normal py-3">شماره پذیرش</th>
                            <th className="font-normal py-3">شماره تماس</th>
                            <th className="font-normal py-3">میانگین امتیاز</th>
                            <th className="font-normal py-3">زمان ثبت</th>
                            <th className="font-normal py-3">شکایت</th>
                            <th className="font-normal py-3"/>
                        </tr>
                    </TableHeader>
                    <TableBody>
                        {
                            data.map((row, index) => (
                                <tr
                                    key={index}
                                    className="text-center text-gray-500 border-b border-gray-150"
                                >
                                    <td className="py-5 font-light">{ row.firstName }</td>
                                    <td className="py-5 font-light">{ row.lastName }</td>
                                    <td className="py-5 font-light">{ row.reception_id }</td>
                                    <td className="py-5 font-light">{ row.phone }</td>
                                    <td className="py-5 font-light">
                                        { row.score === 4 && ( <span className="py-1 px-3 text-sm rounded" style={{ background: '#BDF2D5' }}>عالی</span> ) }
                                        { row.score === 3 && ( <span className="py-1 px-3 text-sm rounded" style={{ background: '#D9F4B3' }}>خوب</span> ) }
                                        { row.score === 2 && ( <span className="py-1 px-3 text-sm rounded" style={{ background: '#FFF2BC' }}>متوسط</span> ) }
                                        { row.score === 1 && ( <span className="py-1 px-3 text-sm rounded" style={{ background: '#FFD5D5' }}>ضعیف</span> ) }
                                    </td>
                                    <td className="py-5 font-light">{ row.date }</td>
                                    <td className="py-5 font-light flex justify-center">
                                        <button>
                                            <Report color={ row.report ? '#FF8989' : '#E9ECF2' } />
                                        </button>
                                    </td>
                                    <td className="py-5 font-light">
                                        <button className="p-3">
                                            <AngleDown/>
                                        </button>
                                    </td>
                                </tr>
                            ))
                        }
                    </TableBody>
                </Table>

                <div className="pt-5 md:pt-8 flex items-center justify-center">
                    <Pagination/>
                </div>
            </Card>
        </PanelPage>
    );
}