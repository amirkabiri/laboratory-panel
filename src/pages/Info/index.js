import React, {useEffect, useState} from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import Button from "components/Button";
import FormGroup from "components/FormGroup";
import Input from "components/Input";
import UploadBox from "components/UploadBox";
import { getCompanyById, updateCompanyById } from "resources/corporations";
import toast from 'shared/toast';

export default function Info(){
    const [company, setCompany] = useState({
        name : '',
        manager : '',
        telephone : '',
        fax : '',
        description : '',
        address : '',
        instagram : '',
        twitter : '',
        facebook : '',
        logoImage : '',
        backGroundImage : ''
    });
    const [fetching, setFetching] = useState(true);
    const [sending, setSending] = useState(false);

    const onChange = key => ({ target }) => setCompany(company => ({
        ...company,
        [key] : target.value
    }));
    const onPhotoChange = key => file => {
        const fileReader = new FileReader();
        fileReader.onload = () => setCompany(company => ({
            ...company,
            [key] : fileReader.result
        }));
        fileReader.readAsDataURL(file);
    };
    const onSubmit = async e => {
        try{
            e.preventDefault();
            setSending(true);

            await updateCompanyById(company.id, company);

            toast.success({
                title : 'موفقیت',
                message : 'کمپانی آپدیت شد'
            });
        }catch (e) {
        }finally {
            setSending(false);
        }
    };

    useEffect(() => {
        getCompanyById(1).then(res => {
            setCompany(res);
            setFetching(false);
        });
    }, []);

    useEffect(() => {
        console.log(company)
    }, [company])

    return (
        <PanelPage
            title="مدیریت اطلاعات آزمایشگاه"
            description="در این بخش باید اطلاعات آزمایشگاه تعریف شود تا در صورتحساب‌ها و قسمت‌های مختلف کیوسک استفاده شود"
            breadcrumb={[{ title : 'مدیریت اطلاعات آزمایشگاه', href : '/info' }]}
        >
            <Card className="p-12" loading={ fetching }>
                <form onSubmit={ onSubmit }>
                    <div className="grid grid-cols-1 grid-rows-2 md:grid-rows-1 md:grid-cols-12 gap-10 mb-12">
                        <div className="col-span-1 row-span-1 md:col-start-1 md:col-end-5 grid grid-cols-1 grid-rows-4 md:grid-rows-3 gap-6">
                            <UploadBox
                                value={ company.logoImage }
                                onChange={ onPhotoChange('logoImage') }
                                title="Logo"
                                className="row-start-1 row-end-3 md:row-span-1"
                            />
                            <UploadBox
                                value={ company.backGroundImage }
                                onChange={ onPhotoChange('backGroundImage') }
                                title="عکس پس زمینه"
                                className="row-start-3 row-end-5 md:row-start-2 md:row-end-4"
                            />
                        </div>
                        <div className="col-span-1 row-span-2 md:col-start-5 md:col-end-13">
                            <FormGroup label="نام آزمایشگاه" className="mb-8">
                                <Input
                                    value={ company.name }
                                    onChange={ onChange('name') }
                                />
                            </FormGroup>
                            <FormGroup label="نام مدیریت" className="mb-8">
                                <Input
                                    value={ company.manager }
                                    onChange={ onChange('manager') }
                                />
                            </FormGroup>
                            <div className="grid grid-cols-2 gap-6 mb-8">
                                <FormGroup label="شماره تماس">
                                    <Input
                                        value={ company.telephone }
                                        onChange={ onChange('telephone') }
                                    />
                                </FormGroup>
                                <FormGroup label="شماره نمابر">
                                    <Input
                                        value={ company.fax }
                                        onChange={ onChange('fax') }
                                    />
                                </FormGroup>
                            </div>
                            <FormGroup label="توضیحات">
                                <Input
                                    value={ company.description }
                                    onChange={ onChange('description') }
                                />
                            </FormGroup>
                        </div>
                    </div>
                    <div className="mb-12">
                        <FormGroup label="آدرس">
                            <Input
                                value={ company.address }
                                onChange={ onChange('address') }
                            />
                        </FormGroup>
                    </div>
                    <div className="grid gap-6 grid-cols-1 md:grid-cols-3 grid-rows-3 md:grid-rows-1 mb-12">
                        <FormGroup label="آدرس صفحه اینستاگرام">
                            <Input
                                value={ company.instagram }
                                onChange={ onChange('instagram') }
                            />
                        </FormGroup>
                        <FormGroup label="آدرس صفحه توییتر">
                            <Input
                                value={ company.twitter }
                                onChange={ onChange('twitter') }
                            />
                        </FormGroup>
                        <FormGroup label="آدرس صفحه فیسبوک">
                            <Input
                                value={ company.facebook }
                                onChange={ onChange('facebook') }
                            />
                        </FormGroup>
                    </div>
                    <div className="flex justify-center flex-col md:flex-row">
                        <Button color="gray" className="w-full md:w-40 px-0 md:ml-2 mb-2 md:mb-0">انصراف</Button>
                        <Button
                            loading={ sending }
                            type="submit"
                            color="success"
                            className="w-full md:w-40 px-0 md:mr-2"
                        >ذخیره اطلاعات</Button>
                    </div>
                </form>
            </Card>
        </PanelPage>
    );
}