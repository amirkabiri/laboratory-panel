import React, { Fragment, useEffect, useState } from 'react';
import PanelPage from "components/PanelPage";
import Card from "components/Card";
import Button from "components/Button";
import SwitchButton from "components/SwitchButton";
import { Link, useParams } from 'react-router-dom';
import RemoveButton from "components/RemoveButton";
import FormGroup from "components/FormGroup";
import Input from "components/Input";
import Label from "components/Label";
import Crown from "icons/Crown";
import CircleCrownPlus from "icons/CircleCrownPlus";
import ArrowRight from "icons/ArrowRight";
import useStationAdminModal from "components/StationAdminModal";

export default function Sections(){
    const { open: openModal } = useStationAdminModal();
    const { id } = useParams();
    const [stations, setStations] = useState([]);

    const onToggleStation = index => () => setStations(stations => stations.map((station, stationIndex) => stationIndex === index ? {
        ...station,
        active : !station.active
    } : station));
    const onStationTitleChange = index => ({ target }) => setStations(stations => stations.map((station, stationIndex) => stationIndex === index ? {
        ...station,
        title : target.value
    } : station));
    const onSelectAdminClick = () => openModal({
        onSave : () => null
    });
    const onAddStation = () => {

    };
    const onSave = () => {

    };
    const onCancel = () => {

    };


    useEffect(() => {
        setStations([
            {
                title : 'صندوق یک',
                active : true,
                admin : 'فاطمه آقا میرزایی'
            },{
                title : 'صندوق دو',
                active : true,
                admin : null
            },{
                title : 'صندوق سه',
                active : false,
                admin : null
            },
        ]);
    }, []);


    return (
        <PanelPage
            title="اضافه کردن ایستگاه"
            description="در این قسمت شما می‌توانید برای هر بخش ایستگاه‌های مربوط به آن را تعریف و برای آن یک مدیر مشخص وآن را فعال یا غیر فعال کنید"
            breadcrumb={[
                { title : 'تنظیمات بخش نوبت دهی', href : '/sections' },
                { title : 'اضافه کردن ایستگاه', href : '/sections/' + id },
            ]}
        >
            <Card padding="p-5 md:p-12 relative">
                <Link to="/sections" className="bg-white rounded-full shadow-lg p-4 absolute" style={{ right : -30 }}>
                    <ArrowRight width={ 25 } height={ 25 }/>
                </Link>

                {
                    stations.map((station, index) => (
                        <Fragment key={ index }>
                            <div className="flex items-end">
                                <FormGroup label="نام ایستگاه" className="flex-1">
                                    <Input
                                        value={ station.title }
                                        onChange={ onStationTitleChange(index) }
                                    />
                                </FormGroup>
                                <div className="flex-1 flex items-center justify-end">
                                    <div className="flex items-center text-gray-500">
                                        { station.active ? 'فعال' : 'غیر فعال' }
                                        <SwitchButton
                                            value={ station.active }
                                            className="mr-3"
                                            onClick={ onToggleStation(index) }
                                        />
                                    </div>
                                    <RemoveButton
                                        className="mr-8"
                                    >حذف ایستگاه</RemoveButton>
                                </div>
                            </div>

                            <div className="pt-5 pb-8">
                                { station.admin && (
                                    <Label className="flex items-center w-1/2">
                                        <Crown className="ml-5" />
                                        { station.admin }
                                    </Label>
                                )}
                                { !station.admin && (
                                    <button
                                        className="flex items-center"
                                        onClick={ onSelectAdminClick }
                                    >
                                        <CircleCrownPlus width={ 30 } height={ 30 }/>
                                        <span className="text-gray-500 mr-3">انتخاب مدیر</span>
                                    </button>
                                )}
                            </div>

                            <div className="border-b border-gray-150 mb-5"/>
                        </Fragment>
                    ))
                }

                <div className="py-10">
                    <button
                        onClick={ onAddStation }
                        className="text-gray-500"
                    >+ اضافه کردن ایستگاه</button>
                </div>

                <div className="flex items-center mt-8 justify-center">
                    <Button
                        onClick={ onCancel }
                        color="gray" className="w-40 ml-4"
                    >انصراف</Button>
                    <Button
                        onClick={ onSave }
                        color="success" className="w-40"
                    >ذخیره اطلاعات</Button>
                </div>
            </Card>
        </PanelPage>
    );
}